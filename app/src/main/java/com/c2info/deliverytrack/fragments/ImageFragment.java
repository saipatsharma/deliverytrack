package com.c2info.deliverytrack.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.NavigationCallback;
import com.c2info.deliverytrack.models.Invoice;
import com.squareup.picasso.Picasso;

/**
 * Created by sai on 2/14/2017.
 */

public class ImageFragment extends Fragment {

    Context context;
    ImageView imageView, leftButton, rightButton;
    String imageUrl;
    int position, size;
    NavigationCallback callback;

    public ImageFragment() {
    }

    public void setImage(Context context, String imageUrl, int position, int size, NavigationCallback navigationCallback) {
        this.context = context;
        this.imageUrl = imageUrl;
        this.position = position;
        this.size = size;
        this.callback = navigationCallback;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        imageView = (ImageView) view.findViewById(R.id.image);
        leftButton = (ImageView) view.findViewById(R.id.left);
        rightButton = (ImageView) view.findViewById(R.id.right);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(imageView != null && imageUrl != null && context != null) {
            Picasso.with(context).load(imageUrl).into(imageView);
        }

        if(position == 0) {
            leftButton.setVisibility(View.INVISIBLE);
        }

        if(position == size - 1) {
            rightButton.setVisibility(View.INVISIBLE);
        }

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position!=0){
                    callback.onLeft();
                }
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position!=size - 1) {
                    callback.onRight();
                }
            }
        });
    }
}
