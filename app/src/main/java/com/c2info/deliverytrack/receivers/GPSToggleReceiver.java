package com.c2info.deliverytrack.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

import com.c2info.deliverytrack.events.GPSToggleEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sai on 2/10/2017.
 */

public class GPSToggleReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                EventBus.getDefault().post(new GPSToggleEvent(true));
            } else {
                EventBus.getDefault().post(new GPSToggleEvent(false));
            }
        }
    }
}
