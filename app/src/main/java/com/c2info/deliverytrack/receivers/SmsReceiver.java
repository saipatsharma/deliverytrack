package com.c2info.deliverytrack.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.RequiresApi;
import android.telephony.SmsMessage;
import android.util.Log;

import com.c2info.deliverytrack.events.OtpReceivedEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sai on 2/16/2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(ACTION)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                StringBuilder buf = new StringBuilder();
                SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                for (int i = 0; i < messages.length; i++) {
                    SmsMessage message = messages[i];
                    buf.append(message.getDisplayOriginatingAddress());
                    buf.append(" - ");
                    buf.append(message.getDisplayMessageBody());

                    if (message.getDisplayOriginatingAddress().contains("LIVCON")) {
                        String otp = message.getDisplayMessageBody().trim().split(" ")[5];
                        EventBus.getDefault().post(new OtpReceivedEvent(otp));
                    }
                }
            }
        }

    }
}
