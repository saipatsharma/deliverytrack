package com.c2info.deliverytrack.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.c2info.deliverytrack.events.InternetToggleEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sai on 2/10/2017.
 */

public class InternetToggleReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equalsIgnoreCase(ConnectivityManager.CONNECTIVITY_ACTION)) {
            if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                EventBus.getDefault().post(new InternetToggleEvent(false));
            } else {
                EventBus.getDefault().post(new InternetToggleEvent(true));
            }
        }
    }
}
