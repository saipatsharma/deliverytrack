package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/15/2017.
 */

public class Reason implements Parcelable {
    private String id, reason, extraReason;

    public Reason(String id, String reason) {
        this.id = id;
        this.reason = reason;
        this.extraReason = "";
    }


    protected Reason(Parcel in) {
        id = in.readString();
        reason = in.readString();
        extraReason = in.readString();
    }

    public static final Creator<Reason> CREATOR = new Creator<Reason>() {
        @Override
        public Reason createFromParcel(Parcel in) {
            return new Reason(in);
        }

        @Override
        public Reason[] newArray(int size) {
            return new Reason[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getExtraReason() {
        return extraReason;
    }

    public void setExtraReason(String extraReason) {
        this.extraReason = extraReason;
    }

    public Reason parse(JSONObject object) {
        String id = "", reason = "";
        try {
            if (object.has("reason_id"))
                id = object.getString("reason_id");
            if (object.has("reason"))
                reason = object.getString("reason");

            return new Reason(id, reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Reason> parse(JSONArray array) {
        List<Reason> reasons = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++)
                reasons.add(parse(array.getJSONObject(i)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reasons;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(reason);
        dest.writeString(extraReason);
    }
}
