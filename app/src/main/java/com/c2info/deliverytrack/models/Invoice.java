package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/9/2017.
 */

public class Invoice implements Parcelable {

    private String invoiceNo = "", capturedImage = "", transDetId;
    private double amount;
    private boolean hasPackages, hasPrescriptions, accepted, rejected, presciptionShown, reasonGiven, rejectionCheck = true;
    private List<String> prescriptionUrls;
    private Reason rejectionReason;
    private List<Package> packages;

    public Invoice(String invoiceNo, double amount, boolean hasPackages, boolean hasPrescriptions, List<String> prescriptionUrls, List<Package> packages) {
        this.invoiceNo = invoiceNo;
        this.amount = amount;
        this.hasPackages = hasPackages;
        this.hasPrescriptions = hasPrescriptions;
        this.prescriptionUrls = prescriptionUrls;
        this.packages = packages;
        this.accepted = true;
        this.rejected = false;
        this.presciptionShown = false;
        this.reasonGiven = false;
        this.rejectionCheck = true;
        capturedImage = "";
    }

    public Invoice(String invoiceNo, String capturedImage, String transDetId) {
        this.invoiceNo = invoiceNo;
        this.capturedImage = capturedImage;
        this.transDetId = transDetId;
        this.rejected = false;
        this.rejectionCheck = true;
    }

    protected Invoice(Parcel in) {
        invoiceNo = in.readString();
        capturedImage = in.readString();
        transDetId = in.readString();
        amount = in.readDouble();
        hasPackages = in.readByte() != 0;
        hasPrescriptions = in.readByte() != 0;
        accepted = in.readByte() != 0;
        rejected = in.readByte() != 0;
        presciptionShown = in.readByte() != 0;
        reasonGiven = in.readByte() != 0;
        rejectionCheck = in.readByte() != 0;
        prescriptionUrls = in.createStringArrayList();
        rejectionReason = in.readParcelable(Reason.class.getClassLoader());
        packages = in.createTypedArrayList(Package.CREATOR);
    }

    public static final Creator<Invoice> CREATOR = new Creator<Invoice>() {
        @Override
        public Invoice createFromParcel(Parcel in) {
            return new Invoice(in);
        }

        @Override
        public Invoice[] newArray(int size) {
            return new Invoice[size];
        }
    };

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean hasPackages() {
        return hasPackages;
    }

    public void setHasPackages(boolean hasPackages) {
        this.hasPackages = hasPackages;
    }

    public boolean hasPrescriptions() {
        return hasPrescriptions;
    }

    public void setHasPrescriptions(boolean hasPrescriptions) {
        this.hasPrescriptions = hasPrescriptions;
    }

    public List<String> getPrescriptionUrls() {
        return prescriptionUrls;
    }

    public void setPrescriptionUrls(List<String> prescriptionUrls) {
        this.prescriptionUrls = prescriptionUrls;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isPresciptionShown() {
        return presciptionShown;
    }

    public void setPresciptionShown(boolean presciptionShown) {
        this.presciptionShown = presciptionShown;
    }

    public boolean isReasonGiven() {
        return reasonGiven;
    }

    public void setReasonGiven(boolean reasonGiven) {
        this.reasonGiven = reasonGiven;
    }

    public Reason getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(Reason rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getTransDetId() {
        return transDetId;
    }

    public void setTransDetId(String transDetId) {
        this.transDetId = transDetId;
    }

    public String getCapturedImage() {
        return capturedImage;
    }

    public void setCapturedImage(String capturedImage) {
        this.capturedImage = capturedImage;
    }

    public boolean isRejectionChecked() {
        return rejectionCheck;
    }

    public void setRejectionChecked(boolean rejectionCheck) {
        this.rejectionCheck = rejectionCheck;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

    private static Invoice parse(JSONObject object) {
        String invoiceNo = "";
        double amount = 0;
        boolean hasPackages = false, hasPrescriptions = false;
        List<String> prescriptionUrls = new ArrayList<>();
        List<Package> packages = new ArrayList<>();
        try {
            if (object.has("invoice_no"))
                invoiceNo = object.getString("invoice_no");
            if (object.has("amount"))
                amount = Double.valueOf(object.getString("amount"));
            if (object.has("package_info"))
                hasPackages = object.getBoolean("package_info");
            if (object.has("prescription"))
                hasPrescriptions = object.getBoolean("prescription");
            if (hasPackages) {
                if (object.has("packages")) {
                    packages = Package.parse(object.getJSONArray("packages"));
                }
            }
            if (hasPrescriptions) {
                if (object.has("prescription_urls")) {
                    JSONArray array = object.getJSONArray("prescription_urls");
                    for (int i = 0; i < array.length(); i++) {
                        prescriptionUrls.add(array.getString(i));
                    }
                }
            }

            return new Invoice(invoiceNo, amount, hasPackages, hasPrescriptions, prescriptionUrls, packages);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Invoice> parse(JSONArray array) {
        ArrayList<Invoice> invoices = new ArrayList<>();
        try {
            for(int i=0; i<array.length(); i++) {
                invoices.add(parse(array.getJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return invoices;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(invoiceNo);
        dest.writeString(capturedImage);
        dest.writeString(transDetId);
        dest.writeDouble(amount);
        dest.writeByte((byte) (hasPackages ? 1 : 0));
        dest.writeByte((byte) (hasPrescriptions ? 1 : 0));
        dest.writeByte((byte) (accepted ? 1 : 0));
        dest.writeByte((byte) (rejected ? 1 : 0));
        dest.writeByte((byte) (presciptionShown ? 1 : 0));
        dest.writeByte((byte) (reasonGiven ? 1 : 0));
        dest.writeByte((byte) (rejectionCheck ? 1 : 0));
        dest.writeStringList(prescriptionUrls);
        dest.writeParcelable(rejectionReason, flags);
        dest.writeTypedList(packages);
    }
}
