package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/9/2017.
 */

public class Package implements Parcelable {

    private String item;
    private double quantity;
    private boolean checked;

    public Package(String item, double quantity) {
        this.item = item;
        this.quantity = quantity;
        this.checked = false;
    }


    protected Package(Parcel in) {
        item = in.readString();
        quantity = in.readDouble();
        checked = in.readByte() != 0;
    }

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @Override
        public Package createFromParcel(Parcel in) {
            return new Package(in);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return (int) quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public static Package parse(JSONObject object) {
        String item="";
        double quantity = 0;
        try {
            if(object.has("item"))
                item = object.getString("item");
            if(object.has("quantity"))
                quantity = Double.valueOf(object.getString("quantity"));

            return new Package(item, quantity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Package> parse(JSONArray array) {
        List<Package> packages = new ArrayList<>();
        try {
            for(int i=0; i<array.length(); i++) {
                Package packageItem = parse(array.getJSONObject(i));
                if(!packageItem.getItem().equalsIgnoreCase("-NA-") && packageItem.getQuantity() > 0)
                    packages.add(parse(array.getJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return packages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(item);
        dest.writeDouble(quantity);
        dest.writeByte((byte) (checked ? 1 : 0));
    }
}
