package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sai on 2/22/2017.
 */

public class Payment implements Parcelable{

    private String method, amount, referenceNumber, imageUrl = "", transId;

    public Payment(String method, String amount, String referenceNumber) {
        this.method = method;
        this.amount = amount;
        this.referenceNumber = referenceNumber;
        this.imageUrl = "";
    }

    public Payment() {
    }

    public Payment(String method, String amount) {
        this.method = method;
        this.amount = amount;
        this.referenceNumber = "";
        this.imageUrl = "";
    }


    protected Payment(Parcel in) {
        method = in.readString();
        amount = in.readString();
        referenceNumber = in.readString();
        imageUrl = in.readString();
        transId = in.readString();
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel in) {
            return new Payment(in);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(method);
        dest.writeString(amount);
        dest.writeString(referenceNumber);
        dest.writeString(imageUrl);
        dest.writeString(transId);
    }


    public class Method {
        public static final String None = "none";
        public static final String Cash = "cash";
        public static final String Cheque = "cheque";
        public static final String Card = "card on delivery";
    }
}
