package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/9/2017.
 */

public class Customer implements Parcelable {
    private String id, name, address, supplierName;
    private int pendingInvoices;
    private boolean hasLocation;

    public Customer(String id, String name, String address, int pendingInvoices, boolean hasLocation, String supplierName) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.pendingInvoices = pendingInvoices;
        this.hasLocation = hasLocation;
        this.supplierName = supplierName;
    }

    public Customer() {
    }

    protected Customer(Parcel in) {
        id = in.readString();
        name = in.readString();
        address = in.readString();
        supplierName = in.readString();
        pendingInvoices = in.readInt();
        hasLocation = in.readByte() != 0;
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPendingInvoices() {
        return pendingInvoices;
    }

    public void setPendingInvoices(int pendingInvoices) {
        this.pendingInvoices = pendingInvoices;
    }

    public boolean hasLocation() {
        return hasLocation;
    }

    public void setHasLocation(boolean hasLocation) {
        this.hasLocation = hasLocation;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    private static Customer parse(JSONObject object) {
        String id = "", name = "", address = "", supplierName = "";
        int pendingInvoices = 0;
        boolean hasLocation = false;
        try {
            if (object.has("customer_id"))
                id = object.getString("customer_id");
            else if(object.has("customer_no"))
                id = object.getString("customer_no");
            if (object.has("customer_name"))
                name = object.getString("customer_name");
            if (object.has("address"))
                address = object.getString("address");
            if (object.has("pending_invoice"))
                pendingInvoices = Integer.parseInt(object.getString("pending_invoice"));
            if (object.has("supplier_name"))
                supplierName = object.getString("supplier_name");
            if(object.has("tagged"))
                hasLocation = object.getBoolean("tagged");

            return new Customer(id, name, address, pendingInvoices, hasLocation, supplierName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Customer> parse(JSONArray array) {
        List<Customer> customers = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                customers.add(parse(array.getJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customers;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(supplierName);
        dest.writeInt(pendingInvoices);
        dest.writeByte((byte) (hasLocation ? 1 : 0));
    }
}
