package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sai on 3/3/2017.
 */

public class CompletedRequest implements Parcelable {
    private String request, images, time;

    public CompletedRequest(String request, String images, String time) {
        this.request = request;
        this.images = images;
        this.time = time;
    }

    protected CompletedRequest(Parcel in) {
        request = in.readString();
        images = in.readString();
        time = in.readString();
    }

    public static final Creator<CompletedRequest> CREATOR = new Creator<CompletedRequest>() {
        @Override
        public CompletedRequest createFromParcel(Parcel in) {
            return new CompletedRequest(in);
        }

        @Override
        public CompletedRequest[] newArray(int size) {
            return new CompletedRequest[size];
        }
    };

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(request);
        dest.writeString(images);
        dest.writeString(time);
    }
}
