package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by sai on 2/22/2017.
 */

public class TrackedLocation implements Parcelable {
    private LatLng latLng;
    private long time;

    public TrackedLocation(LatLng latLng, long time) {
        this.latLng = latLng;
        this.time = time;
    }

    protected TrackedLocation(Parcel in) {
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        time = in.readLong();
    }

    public static final Creator<TrackedLocation> CREATOR = new Creator<TrackedLocation>() {
        @Override
        public TrackedLocation createFromParcel(Parcel in) {
            return new TrackedLocation(in);
        }

        @Override
        public TrackedLocation[] newArray(int size) {
            return new TrackedLocation[size];
        }
    };

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(latLng, flags);
        dest.writeLong(time);
    }
}
