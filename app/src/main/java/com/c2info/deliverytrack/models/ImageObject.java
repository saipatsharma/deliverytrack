package com.c2info.deliverytrack.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/22/2017.
 */

public class ImageObject implements Parcelable {
    private String type = "";
    private Customer customer;
    private String signatureImage = "", customerImage = "", locationImage = "";
    private Payment payment;
    private List<Invoice> invoices = new ArrayList<>();

    public String getImagesString() throws Exception {
        JSONObject object = new JSONObject();
        object.put("type", type);
        object.put("signature_image", signatureImage);
        object.put("customer_image", customerImage);
        object.put("location_image", locationImage);
        if (payment != null)
            object.put("cheque_image", payment.getImageUrl());
        JSONArray array = new JSONArray();
        for (Invoice invoice : invoices) {
            if (invoice.isAccepted()) {
                JSONObject presObj = new JSONObject();
                presObj.put("invoice_no", invoice.getInvoiceNo());
                presObj.put("image", invoice.getCapturedImage());
                array.put(presObj);
            }
        }
        object.put("prescription_images", array);
        return object.toString();
    }

    public ImageObject(JSONObject object) throws Exception {
        JSONObject request = new JSONObject(object.getString("request"));
        customer = new Customer();
        customer.setId(request.getString("customer_id"));
        type = object.getString("type");
        signatureImage = object.getString("signature_image");
        customerImage = object.getString("customer_image");
        locationImage = object.getString("location_image");
        payment = new Payment();
        payment.setMethod(request.getString("payment_method"));
        payment.setImageUrl(object.getString("cheque_image"));
        payment.setTransId(object.getString("trans_id"));
        JSONArray invoicesArray = object.getJSONArray("prescription_images");
        invoices = new ArrayList<>();
        for (int i = 0; i < invoicesArray.length(); i++) {
            JSONObject invoiceObj = invoicesArray.getJSONObject(i);
            String invoiceNo = invoiceObj.getString("invoice_no");
            String transDetId = invoiceObj.getString("trans_det_id");
            String capturedImage = invoiceObj.getString("image");
            invoices.add(new Invoice(invoiceNo, capturedImage, transDetId));
        }
    }

    public ImageObject(Customer customer, String signatureImage, String customerImage, Payment payment, List<Invoice> invoices) {
        this.type = Type.Delivery;
        this.customer = customer;
        this.signatureImage = signatureImage;
        this.customerImage = customerImage;
        this.payment = payment;
        this.invoices = invoices;
    }

    public ImageObject(String locationImage) {
        this.locationImage = locationImage;
        this.type = Type.Location;
    }

    protected ImageObject(Parcel in) {
        type = in.readString();
        customer = in.readParcelable(Customer.class.getClassLoader());
        signatureImage = in.readString();
        customerImage = in.readString();
        locationImage = in.readString();
        payment = in.readParcelable(Payment.class.getClassLoader());
        invoices = in.createTypedArrayList(Invoice.CREATOR);
    }

    public static final Creator<ImageObject> CREATOR = new Creator<ImageObject>() {
        @Override
        public ImageObject createFromParcel(Parcel in) {
            return new ImageObject(in);
        }

        @Override
        public ImageObject[] newArray(int size) {
            return new ImageObject[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public String getLocationImage() {
        return locationImage;
    }

    public void setLocationImage(String locationImage) {
        this.locationImage = locationImage;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeParcelable(customer, flags);
        dest.writeString(signatureImage);
        dest.writeString(customerImage);
        dest.writeString(locationImage);
        dest.writeParcelable(payment, flags);
        dest.writeTypedList(invoices);
    }

    public class Type {
        public static final String Location = "location";
        public static final String Delivery = "delivery";
    }

    public String getBase64(String filePath) {
        File originalFile = new File(filePath);
        String encodedBase64 = "";
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
            byte[] bytes = new byte[(int) originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encode(bytes, Base64.DEFAULT));
            encodedBase64 = "data:image/jpeg;base64," + encodedBase64;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }
}
