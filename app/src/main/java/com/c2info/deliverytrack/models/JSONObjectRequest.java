package com.c2info.deliverytrack.models;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.c2info.deliverytrack.events.LogoutEvent;
import com.c2info.deliverytrack.serverUtils.Urls;
import com.c2info.deliverytrack.utilities.LoginPreferences;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sai on 2/16/2017.
 */

public class JSONObjectRequest extends Request<JSONObject> {

    protected static final String PROTOCOL_CHARSET = "utf-8";
    private Response.Listener<JSONObject> listener;
    private JSONObject jsonRequest;
    private Context context;
    private String url;
    private static final int vno = 100;

    public JSONObjectRequest(Context context, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.listener = listener;
        this.jsonRequest = jsonRequest;
        this.context = context;
        this.url = url;
        try {
            jsonRequest.put("device_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonRequest.put("token", LoginPreferences.getInstance(context).getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        return headers;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            Log.e("Request", url);
            if(jsonRequest.has("no_auth")) {
                if(jsonRequest.getBoolean("no_auth")){
                    jsonRequest.remove("no_auth");
                    Log.e("Request", jsonRequest.toString());
                    return jsonRequest == null ? null : jsonRequest.toString().getBytes(PROTOCOL_CHARSET);
                }
            }
            JSONObject reqObj = new JSONObject();
            LoginPreferences loginPreferences = LoginPreferences.getInstance(context);
            reqObj.put("data", loginPreferences.encrypt(jsonRequest.toString()));
            reqObj.put("token", loginPreferences.getToken());
            reqObj.put("vno", vno);
            Log.e("Request", jsonRequest.toString());
            Log.e("Request", reqObj.toString());
            return jsonRequest == null ? null : reqObj.toString().getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    jsonRequest, PROTOCOL_CHARSET);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            if(response.statusCode == 401 || response.statusCode == 403){
                if(url.equalsIgnoreCase(Urls.FETCH_DELIVERY_CUSTOMERS) ||
                        url.equalsIgnoreCase(Urls.FETCH_INVOICES) ||
                        url.equalsIgnoreCase(Urls.SEARCH_CUSTOMERS) ||
                        url.equalsIgnoreCase(Urls.SEARCH_DELIVERY_CUSTOMERS)) {
                    EventBus.getDefault().post(new LogoutEvent());
                    return null;
                }
            }
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            String res = "";
            try {
                res = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                // Now you can use any deserializer to make sense of data
                Log.e("error", res);
                JSONObject obj = new JSONObject(res);
                Log.e("error", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                Log.e("error", res);
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                Log.e("error", res);
                e2.printStackTrace();
            }
        }
        super.deliverError(error);
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        if (response.has("message")) {
            try {
                if (response.getString("message").equalsIgnoreCase("success")) {
                    LoginPreferences.getInstance(context).extendTime();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.e("Response", response.toString());
        listener.onResponse(response);
    }
}
