package com.c2info.deliverytrack;

import android.*;
import android.Manifest;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.activities.SplashActivity;
import com.c2info.deliverytrack.events.GPSToggleEvent;
import com.c2info.deliverytrack.events.InternetToggleEvent;
import com.c2info.deliverytrack.events.LocationEvent;
import com.c2info.deliverytrack.models.CompletedRequest;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.JSONObjectRequest;
import com.c2info.deliverytrack.serverUtils.RequestHandler;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.serverUtils.Urls;
import com.c2info.deliverytrack.services.DataSyncService;
import com.c2info.deliverytrack.services.ImageUploadService;
import com.c2info.deliverytrack.services.LocationService;
import com.c2info.deliverytrack.services.TrackerService;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/8/2017.
 */

public class MyApplication extends Application {

    private LatLng latLng;
    private Intent trackerIntent, intent, dataSyncService;
    private boolean isConnected = false, isGPSOn = false;
    private boolean isOffline = false;
    private ArrayList<ImageObject> completedImageObjects = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        trackerIntent = new Intent(this, TrackerService.class);
        intent = new Intent(this, LocationService.class);
        dataSyncService = new Intent(this, DataSyncService.class);
        startTracker();
        isConnected = isInternetConnected();
    }

    public LatLng getLatLng() {
        return latLng;
    }

    private void startTracker() {
        int coarseLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int fineLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if ((coarseLocation == PackageManager.PERMISSION_GRANTED) && (fineLocation == PackageManager.PERMISSION_GRANTED)) {
            startService(trackerIntent);
            startService(dataSyncService);
        }
    }

    public void startTracking() {
        startTracker();
        int coarseLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int fineLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if ((coarseLocation == PackageManager.PERMISSION_GRANTED) && (fineLocation == PackageManager.PERMISSION_GRANTED)) {
            startService(intent);
        }
    }

    public void stopTracking() {
        stopService(intent);
    }

    public boolean isConnected() {
        return isConnected;
    }

    public boolean isGPSOn() {
        return isGPSOn;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    @Override
    public void onTerminate() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onTerminate();
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        latLng = new LatLng(event.getLocation().getLatitude(), event.getLocation().getLongitude());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Subscribe
    public void onEvent(GPSToggleEvent event) {
        this.isGPSOn = event.isGpsOn();
    }

    @Subscribe
    public void onEvent(InternetToggleEvent event) {
        this.isConnected = event.isInternetOn();
        if (isConnected) {
            try {
                completedImageObjects = new ArrayList<>();
                checkForCompletedDeliveries();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void checkForCompletedDeliveries() throws Exception {
        List<CompletedRequest> completedRequests = new CustomerDatabase(this).getCompletedDeliveryRequests();
        List<CompletedRequest> locationUpdates = new CustomerDatabase(this).getLocationRequests();
        if (completedRequests.size() > 0) {
            for (CompletedRequest request : completedRequests) {
                completePayment(request);
                return;
            }
        } else if(locationUpdates.size() > 0) {
            for(CompletedRequest request : locationUpdates) {
                completeLocationUpdate(request);
                return;
            }
        } else {
            if(completedImageObjects.size() > 0) {
                uploadImages();
            }
        }
    }

    public void completeLocationUpdate(final CompletedRequest completedRequest) throws Exception {
        JSONObjectRequest request = new JSONObjectRequest(this, Urls.TAG_LOCATION, new JSONObject(completedRequest.getRequest()), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response.has("message")) {
                    try {
                        if(response.getString("message").equalsIgnoreCase("success")) {
                            ImageObject imageObject = new ImageObject(new JSONObject(completedRequest.getImages()).getString("location_image"));
                            Customer customer = new Customer();
                            customer.setId(new JSONObject(completedRequest.getRequest()).getString("customer_id"));
                            imageObject.setCustomer(customer);
                            completedImageObjects.add(imageObject);
                            new CustomerDatabase(MyApplication.this).clearRequest(completedRequest.getTime());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    checkForCompletedDeliveries();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                try {
                    checkForCompletedDeliveries();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        RequestHandler.getInstance(this).addToRequestQueue(request);
    }

    public void completePayment(final CompletedRequest completedRequest) throws Exception {
        JSONObjectRequest request = new JSONObjectRequest(this, Urls.PAYMENT_DETAILS, new JSONObject(completedRequest.getRequest()), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            if (response.has("confirm"))
                                response = response.getJSONObject("confirm");
                            if (response.has("invoice")) {
                                JSONArray detsArray = response.getJSONArray("invoice");
                                JSONObject imageObject = new JSONObject(completedRequest.getImages());
                                JSONArray invoicesArray = imageObject.getJSONArray("prescription_images");
                                String transId = response.has("refno") ? response.getString("refno") : "";
                                imageObject.put("trans_id", transId);
                                for (int i = 0; i < invoicesArray.length(); i++) {
                                    for (int j = 0; j < detsArray.length(); j++) {
                                        JSONObject jsonObject = detsArray.getJSONObject(j);
                                        if (jsonObject.has(invoicesArray.getJSONObject(i).getString("invoice_no"))) {
                                            invoicesArray.getJSONObject(i).put("trans_det_id" ,jsonObject.getString(invoicesArray.getJSONObject(i).getString("invoice_no")));
                                            break;
                                        }
                                    }
                                }
                                imageObject.put("request", completedRequest.getRequest());
                                imageObject.put("time", completedRequest.getTime());
                                completedImageObjects.add(new ImageObject(imageObject));
                                new CustomerDatabase(MyApplication.this).clearRequest(completedRequest.getTime());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                try {
                    checkForCompletedDeliveries();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                try {
                    checkForCompletedDeliveries();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        RequestHandler.getInstance(this).addToRequestQueue(request);
    }

    public void uploadImages() {
        Intent intent = new Intent(this, ImageUploadService.class);
        intent.putExtra("image_objects", completedImageObjects);
        startService(intent);
        if(latLng != null){
            fetchData();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isInternetConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
        } else return false;
    }

    public void fetchData() {
        ServerRequests.getInstance(this).fetchData(latLng, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            new CustomerDatabase(MyApplication.this).saveData(response.getJSONArray("result"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
}
