package com.c2info.deliverytrack.listeners;

import android.location.Location;
import android.os.Bundle;

import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.utilities.LocationUtils;

/**
 * Created by sai on 2/16/2017.
 */

public class DTLocationListener implements android.location.LocationListener {

    private LocationCallBack callBack;

    public DTLocationListener(LocationCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(callBack != null)
            callBack.onLocationUpdate(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
