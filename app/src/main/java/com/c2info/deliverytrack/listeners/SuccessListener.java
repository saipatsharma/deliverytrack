package com.c2info.deliverytrack.listeners;

import android.content.Context;

import com.android.volley.Response;
import com.c2info.deliverytrack.utilities.LoginPreferences;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sai on 2/16/2017.
 */

public class SuccessListener implements Response.Listener<JSONObject> {

    private Context context;

    public SuccessListener(Context context) {
        this.context = context;
    }

    @Override
    public void onResponse(JSONObject response) {
        if(response.has("status")){
            try {
                if(response.getString("status").equalsIgnoreCase("success")) {
                    LoginPreferences.getInstance(context).extendTime();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
