package com.c2info.deliverytrack.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.utilities.CameraUtils;

import java.util.ArrayList;

public class PaymentActivity extends BaseActivity {

    EditText amountEt, referenceNoEt;
    RadioButton cashButton, chequeButton, cardButton;
    TextView proceedButton;
    LinearLayout cardDetailsLayout;
    boolean imageCaptured = false;
    CameraUtils cameraUtils;
    Payment payment;
    String imagePath = "";
    ArrayList<String> paymentMethods = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        amountEt = (EditText) findViewById(R.id.amount);
        referenceNoEt = (EditText) findViewById(R.id.reference);
        cashButton = (RadioButton) findViewById(R.id.cash);
        chequeButton = (RadioButton) findViewById(R.id.cheque);
        cardButton = (RadioButton) findViewById(R.id.card);
        proceedButton = (TextView) findViewById(R.id.proceed);
        cardDetailsLayout = (LinearLayout) findViewById(R.id.card_details);

        paymentMethods = getIntent().getStringArrayListExtra("payment_methods");
        payment = getIntent().getParcelableExtra("payment");
        boolean isCash = false, isCheque = false, isCard = false;
        for (String method : paymentMethods) {
            if (method.equalsIgnoreCase("cash")) {
                isCash = true;
            } else if (method.equalsIgnoreCase("cheque")) {
                isCheque = true;
            } else if(method.equalsIgnoreCase("card on delivery")) {
                isCard = true;
            }
        }

        cashButton.setVisibility(isCash ? View.VISIBLE : View.GONE);
        chequeButton.setVisibility(isCheque ? View.VISIBLE : View.GONE);
        cardButton.setVisibility(isCard ? View.VISIBLE : View.GONE);

        switch (payment.getMethod()){
            case Payment.Method.Cash:
                amountEt.setText(payment.getAmount());
                cashButton.setChecked(true);
                break;
            case Payment.Method.Cheque:
                amountEt.setText(payment.getAmount());
                chequeButton.setChecked(true);
                if(!payment.getImageUrl().equalsIgnoreCase("")) {
                    imageCaptured = true;
                    imagePath = payment.getImageUrl();
                    proceedButton.setVisibility(View.VISIBLE);
                }
                break;
            case Payment.Method.Card:
                cardButton.setChecked(true);
                amountEt.setText(payment.getAmount());
                referenceNoEt.setText(payment.getReferenceNumber());
                break;
            case Payment.Method.None:
                proceedButton.setVisibility(View.GONE);
                break;
        }
        cameraUtils = CameraUtils.getInstance(this);

        cashButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cardDetailsLayout.setVisibility(View.GONE);
                    proceedButton.setVisibility(View.VISIBLE);
                }
            }
        });

        chequeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cardDetailsLayout.setVisibility(View.GONE);
                    proceedButton.setVisibility(View.VISIBLE);
                }
            }
        });

        cardButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cardDetailsLayout.setVisibility(View.VISIBLE);
                    proceedButton.setVisibility(View.VISIBLE);
                }
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amountEt.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(PaymentActivity.this, "Enter a valid amount!", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    double amount = Double.parseDouble(amountEt.getText().toString());
                    if(amount <= 0) {
                        Toast.makeText(PaymentActivity.this, "Amount should be greater than 0!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(PaymentActivity.this, "Enter a valid amount!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(chequeButton.isChecked()){
                    if(imageCaptured) {
                        new AlertDialog.Builder(PaymentActivity.this)
                                .setMessage("Do you want to capture a new Image?")
                                .setCancelable(true)
                                .setPositiveButton("Capture New", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        cameraUtils.captureImage();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        proceedPayment();
                                    }
                                })
                                .show();
                    } else {
                        cameraUtils.captureImage();
                    }
                } else {
                    proceedPayment();
                }
            }
        });
    }

    public void proceedPayment() {
        if (chequeButton.isChecked()) {
            payment = new Payment(Payment.Method.Cheque, amountEt.getText().toString());
            payment.setImageUrl(imagePath);
        }else if (cardButton.isChecked()) {
            if (referenceNoEt.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(PaymentActivity.this, "Enter a valid Reference No.", Toast.LENGTH_SHORT).show();
                return;
            }
            payment = new Payment(Payment.Method.Card, amountEt.getText().toString());
            payment.setReferenceNumber(referenceNoEt.getText().toString());
        } else {
            payment = new Payment(Payment.Method.Cash, amountEt.getText().toString());
        }
        Intent intent =  new Intent();
        intent.putExtra("payment", payment);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraUtils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                imageCaptured = true;
                imagePath = cameraUtils.getImagePath();
                proceedPayment();
            }
        }
    }
}
