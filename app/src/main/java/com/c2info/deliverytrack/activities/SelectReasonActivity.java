package com.c2info.deliverytrack.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.Reason;
import com.c2info.deliverytrack.utilities.DeliveryTrackDB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SelectReasonActivity extends BaseActivity {

    RadioGroup radioGroup;
    EditText reasonEt;
    List<Reason> reasons = new ArrayList<>();
    Customer customer;
    TextView proceedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_reason);

        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        reasonEt = (EditText) findViewById(R.id.reason_text);
        proceedButton = (TextView) findViewById(R.id.proceed);
        makeBold(findViewById(R.id.label));

        customer = getIntent().getParcelableExtra("customer");

        DeliveryTrackDB db = new DeliveryTrackDB(this);
        reasons = db.getReasons(customer.getSupplierName());

        for (int i = 0; i < reasons.size(); i++) {
            if(reasons.get(i).getReason().equalsIgnoreCase("other") || reasons.get(i).getReason().equalsIgnoreCase("others")) {
                Reason reason = reasons.get(i);
                reasons.remove(i);
                reasons.add(reason);
                break;
            }
        }

        for (Reason reason : reasons) {
            RadioButton radioButton = (RadioButton) LayoutInflater.from(this).inflate(R.layout.item_reject_reason, radioGroup, false);
            radioButton.setText(reason.getReason());
            radioButton.setId(reasons.indexOf(reason));
            radioGroup.addView(radioButton);
        }

        applyFont(radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Reason reason = reasons.get(checkedId);
                if (checkedId == -1) {
                    reasonEt.setVisibility(View.GONE);
                    return;
                }
                if (reason.getReason().equalsIgnoreCase("other") || reason.getReason().equalsIgnoreCase("others")) {
                    reasonEt.setText("");
                    reasonEt.setVisibility(View.VISIBLE);
                } else {
                    reasonEt.setVisibility(View.GONE);
                }
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(SelectReasonActivity.this, "Select a reason!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String reason = ((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                Reason rejectionReason = null;
                if (reason.equalsIgnoreCase("other") || reason.equalsIgnoreCase("others")) {
                    if (reasonEt.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(SelectReasonActivity.this, "Enter a valid reason", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        for (Reason reason1 : reasons) {
                            if (reason.equalsIgnoreCase(reason1.getReason())) {
                                rejectionReason = reason1;
                                rejectionReason.setExtraReason(reasonEt.getText().toString());
                                break;
                            }
                        }
                    }
                } else {
                    for (Reason reason1 : reasons) {
                        if (reason.equalsIgnoreCase(reason1.getReason())) {
                            rejectionReason = reason1;
                            break;
                        }
                    }
                }
                Intent intent = new Intent();
                intent.putExtra("reason", rejectionReason);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
