package com.c2info.deliverytrack.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.ImagesPagerAdapter;
import com.c2info.deliverytrack.callbacks.NavigationCallback;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.utilities.CameraUtils;

public class PrescriptionsActivity extends BaseActivity {

    TextView invoiceNoTv, proceedButton, captureButton, rejectButton;
    ViewPager viewPager;
    Invoice invoice;
    CameraUtils cameraUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptions);

        invoiceNoTv = (TextView) findViewById(R.id.invoice_no);
        proceedButton = (TextView) findViewById(R.id.proceed);
        captureButton = (TextView) findViewById(R.id.capture);
        rejectButton = (TextView) findViewById(R.id.reject);
        viewPager = (ViewPager) findViewById(R.id.pager);

        invoice = getIntent().getParcelableExtra("invoice");
        cameraUtils = CameraUtils.getInstance(this);

        invoiceNoTv.setText("Invoice No.: " + invoice.getInvoiceNo());
        NavigationCallback callback = new NavigationCallback() {
            @Override
            public void onLeft() {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
            }

            @Override
            public void onRight() {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            }
        };
        ImagesPagerAdapter adapter = new ImagesPagerAdapter(getSupportFragmentManager(), this, invoice.getPrescriptionUrls(), callback);
        viewPager.setAdapter(adapter);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("invoice", invoice);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("invoice", invoice);
                setResult(RESULT_FIRST_USER, intent);
                finish();
            }
        });

        if(!invoice.getCapturedImage().equalsIgnoreCase("")) {
            captureButton.setText("PROCEED");
        }

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (invoice.getCapturedImage().equalsIgnoreCase("")) {
                    cameraUtils.captureImage();
                } else {
                    new AlertDialog.Builder(PrescriptionsActivity.this)
                            .setMessage("Do you want to capture a new image?")
                            .setPositiveButton("Capture", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cameraUtils.captureImage();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.putExtra("invoice", invoice);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            })
                            .setCancelable(true)
                            .show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraUtils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                invoice.setCapturedImage(cameraUtils.getImagePath());
                Intent intent = new Intent();
                intent.putExtra("invoice", invoice);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
