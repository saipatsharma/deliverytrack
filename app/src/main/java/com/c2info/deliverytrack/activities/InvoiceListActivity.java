
package com.c2info.deliverytrack.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.InvoiceListAdapter;
import com.c2info.deliverytrack.callbacks.InvoiceCheckCallBack;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.Package;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.models.Reason;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.services.ImageUploadService;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InvoiceListActivity extends BaseActivity {

    private static final int PRESCRIPTION_REQ_CODE = 32;
    private static final int PACKAGE_INFO_REQ_CODE = 33;
    private static final int SIGNATURE_REQ_CODE = 34;
    private static final int REJECT_REASON_REQ_CODE = 35;
    private static final int SELECT_REASON_REQ_CODE = 36;
    TextView selectedTv, proceedButton, rejectAllButton;
    RecyclerView recyclerView;
    Customer customer;
    InvoiceListAdapter adapter;
    ArrayList<Invoice> invoices = new ArrayList<>();
    ArrayList<String> paymentMethods = new ArrayList<>();
    InvoiceCheckCallBack callBack;
    MyApplication application;
    ArrayList<Package> newPackages;
    String signatureImage = "", customerImage = "";
    LocationCallBack locationCallBack;
    ProgressDialog progressDialog;
    CheckBox selectAllCB;
    LocationServicesEnabledCallBack enabledCallBack;
    Payment payment;
    boolean rejectAll = false;
    int invoicePointer = 0;
    boolean selectionCheck = false;
    Invoice rejectedInvoice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_list_screen);

        selectedTv = (TextView) findViewById(R.id.selected);
        proceedButton = (TextView) findViewById(R.id.proceed);
        rejectAllButton = (TextView) findViewById(R.id.reject_all);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        selectAllCB = (CheckBox) findViewById(R.id.dummycheck);

        application = (MyApplication) getApplication();
        payment = new Payment(Payment.Method.None, "");

        makeBold(findViewById(R.id.dummy_label));

        callBack = new InvoiceCheckCallBack() {
            @Override
            public void onCheckChanged(ArrayList<Invoice> invoices) {
                InvoiceListActivity.this.invoices = invoices;
                setSelectedText();
            }

            @Override
            public void onClick(Invoice invoice) {
                if (invoice.isAccepted()) {
                    if (invoice.hasPrescriptions()) {
                        if (invoice.getPrescriptionUrls().size() > 0) {
                            Intent intent = new Intent(InvoiceListActivity.this, PrescriptionsActivity.class);
                            intent.putExtra("invoice", invoice);
                            startActivityForResult(intent, PRESCRIPTION_REQ_CODE);
                        }
                    }
                }
            }

            @Override
            public void onReject(Invoice invoice) {
                rejectedInvoice = invoice;
                Intent intent = new Intent(InvoiceListActivity.this, SelectReasonActivity.class);
                intent.putExtra("customer", customer);
                startActivityForResult(intent, SELECT_REASON_REQ_CODE);
            }
        };

        adapter = new InvoiceListAdapter(this, callBack);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        invoices = getIntent().getParcelableArrayListExtra("invoices");
        customer = getIntent().getParcelableExtra("customer");
        paymentMethods = getIntent().getStringArrayListExtra("payment_methods");
        adapter.setInvoices(invoices, true);
        setSelectedText();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean anyInvoiceAccepted = false;
                for (Invoice invoice : invoices) {
                    if (invoices.indexOf(invoice) >= invoicePointer) {
                        if (invoice.isAccepted()) {
                            anyInvoiceAccepted = true;
                            break;
                        }
                    }
                }
                if(anyInvoiceAccepted) {
                    rejectAll = false;
                    checkPrescriptions();
                } else {
                    Toast.makeText(application, "Select atleast one Invoice to proceed!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        selectAllCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (selectionCheck) {
                    selectionCheck = false;
                } else {
                    for (Invoice invoice : invoices) {
                        if (!invoice.isRejected())
                            invoice.setAccepted(isChecked);
                    }
                    adapter.setInvoices(invoices, true);
                }
            }
        });

        rejectAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectAll = true;
                for (Invoice invoice : invoices) {
                    invoice.setAccepted(false);
                }
                adapter.setInvoices(invoices, true);
                checkRejectionReasons();
            }
        });
    }

    public void checkPrescriptions() {
        boolean anyInvoiceAccepted = false;
        for (Invoice invoice : invoices) {
            if (invoices.indexOf(invoice) >= invoicePointer) {
                if (invoice.isAccepted()) {
                    anyInvoiceAccepted = true;
                    if (invoice.hasPrescriptions()) {
                        if (invoice.getPrescriptionUrls().size() > 0) {
                            if (!invoice.isPresciptionShown()) {
                                invoicePointer = invoices.indexOf(invoice);
                                Intent intent = new Intent(this, PrescriptionsActivity.class);
                                intent.putExtra("invoice", invoice);
                                startActivityForResult(intent, PRESCRIPTION_REQ_CODE);
                                return;
                            }
                        }
                    }
                }
            }
        }
        checkForPackageInfo();
    }

    public void checkForPackageInfo() {
        newPackages = new ArrayList<>();
        for (Invoice invoice : invoices) {
            if (invoice.isAccepted()) {
                if (invoice.hasPackages()) {
                    List<Package> oldPackages = invoice.getPackages();
                    for (Package oldPackage : oldPackages) {
                        boolean found = false;
                        for (Package newPackage : newPackages) {
                            if (oldPackage.getItem().equalsIgnoreCase(newPackage.getItem())) {
                                found = true;
                                newPackage.setQuantity(newPackage.getQuantity() + oldPackage.getQuantity());
                            }
                        }
                        if (!found) {
                            newPackages.add(new Package(oldPackage.getItem(), oldPackage.getQuantity()));
                        }
                    }
                }
            }
        }
        if (newPackages.size() > 0) {
            Intent intent = new Intent(this, PackageInfoActivity.class);
            intent.putExtra("packages", newPackages);
            startActivityForResult(intent, PACKAGE_INFO_REQ_CODE);
        } else {
            Intent intent = new Intent(InvoiceListActivity.this, SignatureActivity.class);
            intent.putExtra("payment_methods", paymentMethods);
            intent.putExtra("invoices", invoices);
            intent.putExtra("customer", customer);
            intent.putExtra("packages", newPackages);
            startActivityForResult(intent, SIGNATURE_REQ_CODE);
        }
    }

    public void checkRejectionReasons() {
        ArrayList<Invoice> rejectedInvoices = new ArrayList<>();
        for (Invoice invoice : invoices) {
            if (!invoice.isAccepted() && !invoice.isRejected()) {
                if (!invoice.isReasonGiven()) {
                    rejectedInvoices.add(invoice);
                }
            }
        }
        if (rejectedInvoices.size() > 0) {
            if (rejectedInvoices.size() == invoices.size())
                rejectAll = true;
            Intent intent = new Intent(this, RejectReasonActivity.class);
            intent.putExtra("invoices", rejectedInvoices);
            intent.putExtra("customer", customer);
            intent.putExtra("enable_back", rejectAll);
            startActivityForResult(intent, REJECT_REASON_REQ_CODE);
        } else {
            complete();
        }
    }

    public void complete() {
        progressDialog.setMessage("Finishing up...");
        progressDialog.show();
        if (application.getLatLng() != null) {
            completeRequest(application.getLatLng());
        } else {
            locationCallBack = new LocationCallBack() {
                @Override
                public void onLocationUpdate(Location location) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    completeRequest(latLng);
                }
            };
            enabledCallBack = new LocationServicesEnabledCallBack() {
                @Override
                public void onEnabled() {
                    LocationUtils.getSingleLocation(InvoiceListActivity.this, locationCallBack, enabledCallBack);
                }
            };
            LocationUtils.getSingleLocation(InvoiceListActivity.this, locationCallBack, enabledCallBack);
        }
    }

    public void completeRequest(LatLng latLng) {
        if (!application.isOffline() && application.isConnected()) {
            ServerRequests.getInstance(this).updatePayment(customer, invoices, payment, latLng, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                int acceptedLength = 0;
                                for (Invoice invoice : invoices) {
                                    if (invoice.isAccepted())
                                        acceptedLength++;
                                }
                                if (acceptedLength > 0) {
                                    if (response.has("confirm"))
                                        response = response.getJSONObject("confirm");
                                    payment.setTransId(response.has("refno") ? response.getString("refno") : "");
                                    if (response.has("invoice")) {
                                        JSONArray detsArray = response.getJSONArray("invoice");
                                        for (Invoice invoice : invoices) {
                                            if (invoice.isAccepted()) {
                                                for (int i = 0; i < detsArray.length(); i++) {
                                                    JSONObject jsonObject = detsArray.getJSONObject(i);
                                                    if (jsonObject.has(invoice.getInvoiceNo())) {
                                                        invoice.setTransDetId(jsonObject.getString(invoice.getInvoiceNo()));
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    uploadImages();
                                } else {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    error.printStackTrace();
                    Toast.makeText(application, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            ImageObject imageObject = new ImageObject(customer, signatureImage, customerImage, payment, invoices);
            new CustomerDatabase(this).saveCompletedDelivery(customer, invoices, payment, latLng, imageObject);
            Toast.makeText(application, "waiting for internet!", Toast.LENGTH_SHORT).show();
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            setResult(RESULT_OK);
            finish();
        }
    }

    public void uploadImages() {
        ImageObject imageObject = new ImageObject(customer, signatureImage, customerImage, payment, invoices);
        Intent intent = new Intent(this, ImageUploadService.class);
        intent.putExtra("image_object", imageObject);
        startService(intent);
        setResult(RESULT_OK);
        finish();
    }

    public void processRejectedReasons(ArrayList<Invoice> invoices) {
        for (Invoice invoice : invoices) {
            for (Invoice invoice1 : this.invoices) {
                if (!invoice1.isAccepted()) {
                    if (invoice.getInvoiceNo().equalsIgnoreCase(invoice1.getInvoiceNo())) {
                        invoice1.setReasonGiven(true);
                        invoice1.setRejected(true);
                        invoice1.setRejectionReason(invoice.getRejectionReason());
                        break;
                    }
                }
            }
        }
        complete();
    }

    public void setSelectedText() {
        int selected = 0;
        for (Invoice invoice : invoices) {
            if (invoice.isAccepted()) {
                selected++;
            }
        }
        if (selected == invoices.size()) {
            if (!selectAllCB.isChecked()) {
                selectionCheck = true;
                selectAllCB.setChecked(true);
            }
        } else {
            if (selectAllCB.isChecked()) {
                selectionCheck = true;
                selectAllCB.setChecked(false);
            }
        }
        selectedTv.setText(String.valueOf(selected) + " out of " + String.valueOf(invoices.size()) + " selected");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PRESCRIPTION_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Invoice shownInvoice = data.getParcelableExtra("invoice");
                for (Invoice invoice : invoices) {
                    if (invoice.getInvoiceNo().equalsIgnoreCase(shownInvoice.getInvoiceNo())) {
                        invoice.setPresciptionShown(true);
                        invoice.setCapturedImage(shownInvoice.getCapturedImage());
                    }
                }
                checkPrescriptions();
            } else if (resultCode == RESULT_FIRST_USER) {
                Invoice shownInvoice = data.getParcelableExtra("invoice");
                for (Invoice invoice : invoices) {
                    if (invoice.getInvoiceNo().equalsIgnoreCase(shownInvoice.getInvoiceNo())) {
                        invoice.setAccepted(false);
                        rejectedInvoice = invoice;
                        Intent intent = new Intent(InvoiceListActivity.this, SelectReasonActivity.class);
                        intent.putExtra("customer", customer);
                        startActivityForResult(intent, SELECT_REASON_REQ_CODE);
                    }
                }
//                checkPrescriptions();
//                adapter.setInvoices(invoices, true);
            } else if (resultCode == RESULT_CANCELED) {
                for (Invoice invoice : invoices) {
                    invoice.setPresciptionShown(false);
                }
            }
        } else if (requestCode == PACKAGE_INFO_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(InvoiceListActivity.this, SignatureActivity.class);
                intent.putExtra("payment_methods", paymentMethods);
                intent.putExtra("invoices", invoices);
                intent.putExtra("packages", newPackages);
                intent.putExtra("customer", customer);
                startActivityForResult(intent, SIGNATURE_REQ_CODE);
            } else if (resultCode == RESULT_CANCELED) {
                for (Invoice invoice : invoices) {
                    invoice.setPresciptionShown(false);
                }
            }
        } else if (requestCode == SIGNATURE_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                signatureImage = data.getStringExtra("signature_image");
                customerImage = data.getStringExtra("customer_image");
                payment = data.getParcelableExtra("payment");
                complete();
            } else if (resultCode == RESULT_CANCELED) {
                for (Invoice invoice : invoices) {
                    invoice.setPresciptionShown(false);
                }
            }
        } else if (requestCode == REJECT_REASON_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                ArrayList<Invoice> invoices = data.getParcelableArrayListExtra("invoices");
                processRejectedReasons(invoices);
            }
        } else if (requestCode == SELECT_REASON_REQ_CODE) {
            for (Invoice invoice : invoices) {
                if (invoice.getInvoiceNo().equalsIgnoreCase(rejectedInvoice.getInvoiceNo())) {
                    if (resultCode == RESULT_OK) {
                        Reason reason = data.getParcelableExtra("reason");
                        invoice.setAccepted(false);
                        invoice.setRejected(true);
                        invoice.setReasonGiven(true);
                        invoice.setRejectionReason(reason);
                    } else {
                        invoice.setAccepted(true);
                        invoice.setRejected(false);
                    }
                    break;
                }
            }
            adapter.setInvoices(invoices, true);
            rejectedInvoice = null;
        }
    }
}
