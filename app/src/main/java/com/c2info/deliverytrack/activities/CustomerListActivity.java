package com.c2info.deliverytrack.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.CustomerListAdapter;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.events.LogoutEvent;
import com.c2info.deliverytrack.listeners.ScrollListener;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.KeyboardManager;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CustomerListActivity extends BaseActivity {

    EditText searchEt;
    RecyclerView recyclerView;
    CustomerListAdapter adapter;
    List<Customer> customers = new ArrayList<>(), fetchedCustomers = new ArrayList<>();
    LocationCallBack locationCallBack;
    LocationServicesEnabledCallBack enabledCallBack;
    ProgressDialog progressDialog;
    boolean isSearch = false;
    int page = 1, perPage = 10;
    MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);

        EventBus.getDefault().register(this);

        searchEt = (EditText) findViewById(R.id.query);
        recyclerView = (RecyclerView) findViewById(R.id.list);

        application = (MyApplication) getApplication();

        adapter = new CustomerListAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                page = 1;
                searchCustomers(searchEt.getText().toString(), page);
                KeyboardManager.hideKeyboard(CustomerListActivity.this);
                return true;
            }
        });

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                page = 1;
                if (s.length() == 0) {
                    isSearch = false;
                    customers = fetchedCustomers;
                } else {
                    isSearch = true;
                    customers = new ArrayList<Customer>();
                    if(s.length() > 2) {
                        searchCustomers(searchEt.getText().toString(), page);
                    }
                }
                adapter.setCustomers(customers);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.addOnScrollListener(new ScrollListener(mLayoutManager){
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                if (customers.size() >= perPage && customers.size() >= perPage * CustomerListActivity.this.page) {
                    CustomerListActivity.this.page++;
                    if(isSearch)
                        searchCustomers(searchEt.getText().toString(), CustomerListActivity.this.page);
                    else
                        fetchCustomers(CustomerListActivity.this.page);
                }
                return false;
            }
        });

        fetchCustomers(page);

    }

    public void fetchCustomers(final int page) {
        progressDialog.setMessage("Fetching Customers...");
        progressDialog.show();
        if(application.isConnected()) {
            application.setOffline(false);
            if (application.getLatLng() != null) {
                fetchRequest(application.getLatLng(), page);
            } else {
                locationCallBack = new LocationCallBack() {
                    @Override
                    public void onLocationUpdate(Location location) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        fetchRequest(latLng, page);
                    }
                };
                enabledCallBack = new LocationServicesEnabledCallBack() {
                    @Override
                    public void onEnabled() {
                        LocationUtils.getSingleLocation(CustomerListActivity.this, locationCallBack, enabledCallBack);
                    }
                };
                LocationUtils.getSingleLocation(this, locationCallBack, enabledCallBack);
            }
        } else {
            if(page == 1) {
                application.setOffline(true);
                customers = new CustomerDatabase(this).getCustomers();
                fetchedCustomers = customers;
                adapter.setCustomers(customers);
            }
            if(progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public void fetchRequest(LatLng latLng, final int page) {
        ServerRequests.getInstance(CustomerListActivity.this).fetchDeliveryCustomers(latLng, page, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            if (page == 1) {
                                customers = Customer.parse(response.getJSONArray("result"));
                            } else {
                                customers.addAll(Customer.parse(response.getJSONArray("result")));
                            }
                            fetchedCustomers = new ArrayList<Customer>();
                            for(Customer customer : customers) {
                                if(customer.getPendingInvoices() > 0) {
                                    fetchedCustomers.add(customer);
                                }
                            }
                            customers = fetchedCustomers;
                            adapter.setCustomers(customers);
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        customers = new ArrayList<Customer>();
                        adapter.setCustomers(customers);
                    }
                }
                Toast.makeText(CustomerListActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(CustomerListActivity.this, "Somethng went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void searchCustomers(final String searchText, final int page) {
        if(!application.isOffline()) {
            if (application.getLatLng() != null) {
                searchRequest(application.getLatLng(), searchText, page);
            } else {
                locationCallBack = new LocationCallBack() {
                    @Override
                    public void onLocationUpdate(Location location) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        searchRequest(latLng, searchText, page);
                    }
                };
                enabledCallBack = new LocationServicesEnabledCallBack() {
                    @Override
                    public void onEnabled() {
                        LocationUtils.getSingleLocation(CustomerListActivity.this, locationCallBack, enabledCallBack);
                    }
                };
                LocationUtils.getSingleLocation(this, locationCallBack, enabledCallBack);
            }
        } else {
            if(page == 1) {
                List<Customer> allCustomers = new CustomerDatabase(this).getCustomers();
                customers = new ArrayList<>();
                for (Customer customer : allCustomers) {
                    if (customer.getName().toUpperCase().contains(searchText.toUpperCase())) {
                        customers.add(customer);
                    }
                }
                adapter.setCustomers(customers);
            }
        }
    }

    public void searchRequest(final LatLng latLng, final String searchText, final int page) {
        ServerRequests.getInstance(CustomerListActivity.this).searchDeliveryCustomers(searchText, latLng, page, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            if (page == 1) {
                                customers = Customer.parse(response.getJSONArray("result"));
                            } else {
                                customers.addAll(Customer.parse(response.getJSONArray("result")));
                            }
                            adapter.setCustomers(customers);
                        } else {
                            Toast.makeText(CustomerListActivity.this, "Error from API", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        customers = new ArrayList<Customer>();
                        adapter.setCustomers(customers);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(CustomerListActivity.this, "Somethng went wrong!", Toast.LENGTH_SHORT).show();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        Log.e("error", obj.toString());
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CustomerListAdapter.INVOICE_LIST_REQ_CODE){
            if(resultCode == RESULT_OK){
                isSearch = false;
                page = 1;
                fetchCustomers(page);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(LogoutEvent event) {

        setResult(RESULT_FIRST_USER);
        finish();
    }
}
