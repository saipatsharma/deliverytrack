package com.c2info.deliverytrack.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.HiddenCameraActivity;
import com.androidhiddencamera.config.CameraFacing;
import com.androidhiddencamera.config.CameraImageFormat;
import com.androidhiddencamera.config.CameraResolution;
import com.androidhiddencamera.config.CameraRotation;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.PhotoCapturedCallback;
import com.c2info.deliverytrack.callbacks.SignatureCallback;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.Package;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.utilities.DrawingView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SignatureActivity extends HiddenCameraActivity {

    private static final int PAYMENT_REQ_CODE = 32;
    DrawingView drawingView;
    ImageView proceedButton, refreshButton;
    ArrayList<String> paymentMethods = new ArrayList<>();
    RelativeLayout captureView;
    TextView paymentButton, bgTextView;
    ArrayList<Invoice> invoices = new ArrayList<>();
    ArrayList<Package> packages = new ArrayList<>();
    String bgText = "", cameraImage = "", customerName = "";
    Customer customer;
    Payment payment;

    CameraConfig cameraConfig;

    boolean captureInit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

        drawingView = (DrawingView) findViewById(R.id.canvas);
        proceedButton = (ImageView) findViewById(R.id.proceed);
        refreshButton = (ImageView) findViewById(R.id.refresh);
        paymentButton = (TextView) findViewById(R.id.payment);
        bgTextView = (TextView) findViewById(R.id.text);
        captureView = (RelativeLayout) findViewById(R.id.capture_view);

        paymentMethods = getIntent().getStringArrayListExtra("payment_methods");
        invoices = getIntent().getParcelableArrayListExtra("invoices");
        packages = getIntent().getParcelableArrayListExtra("packages");
        customer = getIntent().getParcelableExtra("customer");

        payment = new Payment(Payment.Method.None, "");
        PhotoCapturedCallback capturedCallback = new PhotoCapturedCallback() {
            @Override
            public void onCapture(String imagePath) {
                cameraImage = imagePath;
                captureInit = false;
            }
        };

        cameraConfig = new CameraConfig()
                .getBuilder(this)
                .setCameraFacing(CameraFacing.FRONT_FACING_CAMERA)
                .setCameraResolution(CameraResolution.MEDIUM_RESOLUTION)
                .setImageFormat(CameraImageFormat.FORMAT_JPEG)
                .setImageRotation(CameraRotation.ROTATION_270)
                .build();

        startCamera(cameraConfig);

        SignatureCallback signatureCallback = new SignatureCallback() {
            @Override
            public void onSignature() {
                if (!captureInit && cameraImage.equalsIgnoreCase("")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                captureInit = true;
                                takePicture();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).run();
                }
            }
        };
        drawingView.setCallback(signatureCallback);
        drawingView.setContainer(captureView);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.getDefault());
        bgText += sdf.format(Calendar.getInstance().getTime()) + "\n";
        bgText += "Supplier: " + customer.getSupplierName() + "\n\n";
        bgText += "Invoices No.s:\n";
        int start = 0;
        List<String> addedInvoices = new ArrayList<>();
        for (int i = 0; i < invoices.size(); i++) {
            if (invoices.get(i).isAccepted()) {
                bgText += invoices.get(i).getInvoiceNo();
                addedInvoices.add(invoices.get(i).getInvoiceNo());
                start = i + 1;
                break;
            }
        }
        int j = 0;
        if (invoices.size() > start) {
            for (int i = start; i < invoices.size(); i++) {
                if (invoices.get(i).isAccepted()) {
                    if (!addedInvoices.contains(invoices.get(i).getInvoiceNo())) {
                        j++;
                        bgText += ", " + (j % 3 == 0 ? "\n" : "") + invoices.get(i).getInvoiceNo();
                        addedInvoices.add(invoices.get(i).getInvoiceNo());
                    }
                }
            }
        }
        bgText += "\n\n";
        for (Package packageInfo : packages) {
            bgText += packageInfo.getItem().toUpperCase() + ": " + packageInfo.getQuantity() + "\n";
        }
        customerName = "\n" + customer.getName() + "\n" + customer.getAddress();
        bgTextView.setText(bgText + customerName);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraImage = "";
                captureInit = false;
                drawingView.refresh();
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                drawingView.setBackgroundColor(ActivityCompat.getColor(SignatureActivity.this, R.color.white));
                String imagePath = drawingView.saveAndGetPath();
                if (imagePath == null) {
                    Toast.makeText(SignatureActivity.this, "Please Sign!", Toast.LENGTH_SHORT).show();
                    drawingView.setBackgroundColor(ActivityCompat.getColor(SignatureActivity.this, R.color.transparent));
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("signature_image", imagePath);
                intent.putExtra("customer_image", cameraImage);
                intent.putExtra("payment", payment);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignatureActivity.this, PaymentActivity.class);
                intent.putExtra("payment_methods", paymentMethods);
                intent.putExtra("payment", payment);
                startActivityForResult(intent, PAYMENT_REQ_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                payment = data.getParcelableExtra("payment");
                paymentButton.setText("Change Payment Details");
                String completeText = bgText + "\n";
                completeText += "Amount Paid (" + payment.getMethod().toUpperCase() + "): Rs." + payment.getAmount() + "\n";
                if (payment.getMethod().equalsIgnoreCase(Payment.Method.Card)) {
                    completeText += "Reference Number: " + payment.getReferenceNumber() + "\n";
                }
                bgTextView.setText(completeText + customerName);
            }
        }
    }

    @Override
    public void onImageCapture(@NonNull File imageFile) {
        Log.e("Camera", imageFile.getAbsolutePath());
        cameraImage = Uri.fromFile(imageFile).getPath();
        captureInit = false;
    }

    @Override
    public void onCameraError(int errorCode) {
        Log.e("Camera error", "error " + errorCode);
    }
}
