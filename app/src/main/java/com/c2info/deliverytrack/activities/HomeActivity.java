package com.c2info.deliverytrack.activities;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.services.LocationService;
import com.c2info.deliverytrack.utilities.LoginPreferences;

public class HomeActivity extends BaseActivity {

    private static final int ACTIVITY_CALL_REQ_CODE = 122;
    private static final int TAG_ACTIVITY_REQ_CODE = 123;
    TextView deliverButton, tagButton, logoutButton;
    MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        deliverButton = (TextView) findViewById(R.id.deliver);
        tagButton = (TextView) findViewById(R.id.tag);
        logoutButton = (TextView) findViewById(R.id.logout);

        application = (MyApplication) getApplication();

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        deliverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CustomerListActivity.class);
                startActivityForResult(intent, ACTIVITY_CALL_REQ_CODE);
            }
        });

        tagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, TagLocationActivity.class);
                startActivityForResult(intent, TAG_ACTIVITY_REQ_CODE);
            }
        });

        if(LoginPreferences.getInstance(this).canTag()){
            tagButton.setVisibility(View.VISIBLE);
        } else {
            tagButton.setVisibility(View.GONE);
        }
    }

    public void logout() {
        LoginPreferences.getInstance(HomeActivity.this).logout();
        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ACTIVITY_CALL_REQ_CODE) {
            if(resultCode == RESULT_FIRST_USER) {
                logout();
            }
        } else if(requestCode == TAG_ACTIVITY_REQ_CODE) {
            if(resultCode == RESULT_OK) {
                Intent intent = new Intent(HomeActivity.this, TagLocationActivity.class);
                startActivityForResult(intent, TAG_ACTIVITY_REQ_CODE);
            } else if(resultCode == RESULT_FIRST_USER) {
                logout();
            }
        }
    }

    @Override
    protected void onDestroy() {
        application.stopTracking();
        super.onDestroy();
    }
}
