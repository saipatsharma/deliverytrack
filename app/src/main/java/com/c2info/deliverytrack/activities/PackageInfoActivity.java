package com.c2info.deliverytrack.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.PackageListAdapter;
import com.c2info.deliverytrack.callbacks.PackageCheckCallBack;
import com.c2info.deliverytrack.models.Package;

import java.util.ArrayList;
import java.util.List;

public class PackageInfoActivity extends BaseActivity {

    RecyclerView recyclerView;
    TextView proceedButton;
    LinearLayout bottomLayout;
    PackageListAdapter adapter;
    List<Package> packages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_info);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        proceedButton = (TextView) findViewById(R.id.proceed);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        packages = getIntent().getParcelableArrayListExtra("packages");

        PackageCheckCallBack callBack = new PackageCheckCallBack() {
            @Override
            public void onCheckChanged(List<Package> packages) {
                PackageInfoActivity.this.packages = packages;
                checkForPackages();
            }
        };

        adapter = new PackageListAdapter(this, callBack);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        adapter.setPackages(packages);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

    }

    public void checkForPackages(){
        boolean allChecked = true;
        for(Package packageInfo: packages) {
            if(!packageInfo.isChecked()){
                allChecked = false;
                break;
            }
        }
        if(allChecked) {
            bottomLayout.setVisibility(View.VISIBLE);
        } else {
            bottomLayout.setVisibility(View.GONE);
        }
    }
}
