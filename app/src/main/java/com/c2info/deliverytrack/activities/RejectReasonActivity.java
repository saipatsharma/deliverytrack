package com.c2info.deliverytrack.activities;

import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.RejectedInvoiceAdapter;
import com.c2info.deliverytrack.callbacks.RejectedInvoiceCheckCallBack;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.Reason;
import com.c2info.deliverytrack.utilities.DeliveryTrackDB;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class RejectReasonActivity extends BaseActivity {

    private static final int SELECT_REASON_REQ_CODE = 322;
    RecyclerView recyclerView;
    TextView proceedButton;
    RejectedInvoiceAdapter adapter;
    RejectedInvoiceCheckCallBack callBack;
    Customer customer;
    CheckBox selectAllCB;
    boolean selectCheck = false, selectAllClick = false;
    ArrayList<Invoice> invoices = new ArrayList<>(), selectedInvoices = new ArrayList<>(), rejectedInvoices = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_reason);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        proceedButton = (TextView) findViewById(R.id.proceed);
        selectAllCB = (CheckBox) findViewById(R.id.select_all);
        makeBold(findViewById(R.id.label));

        invoices = getIntent().getParcelableArrayListExtra("invoices");
        customer = getIntent().getParcelableExtra("customer");

        callBack = new RejectedInvoiceCheckCallBack() {
            @Override
            public void onCheckChanged(Invoice invoice, boolean isChecked) {
                if (isChecked) {
                    boolean found = false;
                    for (Invoice invoice1 : selectedInvoices) {
                        if (invoice1.getInvoiceNo().equalsIgnoreCase(invoice.getInvoiceNo())) {
                            found = true;
                            break;
                        }
                    }
                    if(!found) {
                        selectedInvoices.add(invoice);
                    }
                } else {
                    for (Invoice invoice1 : selectedInvoices) {
                        if (invoice1.getInvoiceNo().equalsIgnoreCase(invoice.getInvoiceNo())) {
                            selectedInvoices.remove(invoice1);
                            break;
                        }
                    }
                }
                if(!selectAllClick) {
                    if (selectedInvoices.size() == rejectedInvoices.size()) {
                        if (!selectAllCB.isChecked()) {
                            selectCheck = true;
                            selectAllCB.setChecked(true);
                        }
                    } else {
                        if (selectAllCB.isChecked()) {
                            selectCheck = true;
                            selectAllCB.setChecked(false);
                        }
                    }
                }
            }
        };

        selectAllCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectAllClick = true;
                if (selectCheck) {
                    selectCheck = false;
                } else {
                    selectedInvoices = new ArrayList<>();
                    if (isChecked) {
                        selectedInvoices.addAll(rejectedInvoices);
                    }
                    for (Invoice invoice : rejectedInvoices) {
                        invoice.setRejectionChecked(isChecked);
                    }
                    adapter.setInvoices(rejectedInvoices);
                }
                new CountDownTimer(500,500) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        selectAllClick = false;
                    }
                }.start();
            }
        });

        checkForReasons();

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedInvoices.size() == 0) {
                    Toast.makeText(RejectReasonActivity.this, "Select Invoice(s)", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(RejectReasonActivity.this, SelectReasonActivity.class);
                intent.putExtra("customer", customer);
                startActivityForResult(intent, SELECT_REASON_REQ_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_REASON_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Reason reason = data.getParcelableExtra("reason");
                appendReason(reason);
            }
        }
    }

    public void checkForReasons() {
        rejectedInvoices = new ArrayList<>();
        for (Invoice invoice : invoices) {
            if (!invoice.isAccepted() && !invoice.isReasonGiven()) {
                rejectedInvoices.add(invoice);
            }
        }
        if (rejectedInvoices.size() > 0) {
            adapter = new RejectedInvoiceAdapter(this, callBack);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
            adapter.setInvoices(rejectedInvoices);
            selectedInvoices = new ArrayList<>();
            selectedInvoices.addAll(rejectedInvoices);
        } else {
            Intent intent = new Intent();
            intent.putExtra("invoices", invoices);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void appendReason(Reason reason) {
        for (Invoice invoice : selectedInvoices) {
            for (Invoice originalInvoice : invoices) {
                if (!originalInvoice.isAccepted() && !originalInvoice.isReasonGiven()) {
                    if (invoice.getInvoiceNo().equalsIgnoreCase(originalInvoice.getInvoiceNo())) {
                        originalInvoice.setRejected(true);
                        originalInvoice.setRejectionReason(reason);
                        originalInvoice.setReasonGiven(true);
                        break;
                    }
                }
            }
        }
        checkForReasons();
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra("enable_back", false))
            super.onBackPressed();
    }
}
