package com.c2info.deliverytrack.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.adapters.CustomerListAdapter;
import com.c2info.deliverytrack.adapters.TaggedCustomerListAdapter;
import com.c2info.deliverytrack.callbacks.CustomerSelectCallBack;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.events.LogoutEvent;
import com.c2info.deliverytrack.listeners.ScrollListener;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.services.ImageUploadService;
import com.c2info.deliverytrack.utilities.CameraUtils;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.KeyboardManager;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

public class TagLocationActivity extends BaseActivity {

    TextView latitudeTv, longitudeTv, nameTv, addressTv, updateButton, addressFetchedTv;
    ScrollView detailsLayout;
    CameraUtils cameraUtils;
    LocationCallBack locationCallBack;
    LinearLayout bottomLayout;
    TaggedCustomerListAdapter adapter;
    EditText searchEt;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    List<Customer> customers = new ArrayList<>();
    LocationServicesEnabledCallBack enabledCallBack;
    CustomerSelectCallBack customerSelectCallBack;
    Customer customer;
    LatLng latLng;
    String imagePath = "";
    int page = 1, perPage = 10;
    MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_location);

        EventBus.getDefault().register(this);

        latitudeTv = (TextView) findViewById(R.id.latitude);
        longitudeTv = (TextView) findViewById(R.id.longitude);
        searchEt = (EditText) findViewById(R.id.query);
        nameTv = (TextView) findViewById(R.id.name);
        addressTv = (TextView) findViewById(R.id.address);
        updateButton = (TextView) findViewById(R.id.update);
        detailsLayout = (ScrollView) findViewById(R.id.details_layout);
        addressFetchedTv = (TextView) findViewById(R.id.address_fetched);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        cameraUtils = CameraUtils.getInstance(TagLocationActivity.this);

        application = (MyApplication) getApplication();

        customerSelectCallBack = new CustomerSelectCallBack() {
            @Override
            public void onSelect(Customer customer) {
                KeyboardManager.hideKeyboard(TagLocationActivity.this);
                TagLocationActivity.this.customer = customer;
                setCustomerDetails();
            }
        };

        adapter = new TaggedCustomerListAdapter(this, customerSelectCallBack);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        locationCallBack = new LocationCallBack() {
            @Override
            public void onLocationUpdate(Location location) {
                if (latLng == null && progressDialog.isShowing())
                    progressDialog.dismiss();
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
                latitudeTv.setText(String.valueOf(location.getLatitude()) + ", ");
                longitudeTv.setText(String.valueOf(location.getLongitude()));
                getPlace();
            }
        };

        enabledCallBack = new LocationServicesEnabledCallBack() {
            @Override
            public void onEnabled() {
                LocationUtils.getLocationUpdates(TagLocationActivity.this, locationCallBack, enabledCallBack);
            }
        };

        if (application.getLatLng() != null) {
//            progressDialog.setMessage("Waiting for location...");
//            progressDialog.show();
            latLng = application.getLatLng();
            latitudeTv.setText(String.valueOf(latLng.latitude) + ", ");
            longitudeTv.setText(String.valueOf(latLng.longitude));
            getPlace();
        }

        LocationUtils.getLocationUpdates(this, locationCallBack, enabledCallBack);


        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                page = 1;
                searchCustomers(searchEt.getText().toString(), page);
                KeyboardManager.hideKeyboard(TagLocationActivity.this);
                return true;
            }
        });

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customers = new ArrayList<Customer>();
                page = 1;
                adapter.setCustomers(customers);
                detailsLayout.setVisibility(View.GONE);
                bottomLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                if (s.length() > 2) {
                    searchCustomers(s.toString(), page);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.addOnScrollListener(new ScrollListener(mLayoutManager) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                if (customers.size() >= perPage && customers.size() >= perPage * TagLocationActivity.this.page) {
                    TagLocationActivity.this.page++;
                    searchCustomers(searchEt.getText().toString(), TagLocationActivity.this.page);
                }
                return false;
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (latLng != null && customer != null) {
                    new AlertDialog.Builder(TagLocationActivity.this)
                            .setMessage(imagePath.equalsIgnoreCase("") ? "Do you want to capture an image?" : "Do you want to capture a new image?")
                            .setPositiveButton("Capture", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cameraUtils.captureImage();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    updateLocation();
                                }
                            })
                            .setCancelable(true)
                            .show();
                }
            }
        });

    }

    private void getPlace() {
        ServerRequests.getInstance(this).getPlace(this, latLng, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject resObj) {
                try {
                    if (resObj.has("results")) {
                        Log.e("LOCATION API", resObj.toString());
                        JSONArray array = resObj.getJSONArray("results");
                        if (array.length() > 0) {
                            JSONObject obj = array.getJSONObject(0);
                            String address = obj.getString("formatted_address");
                            addressFetchedTv.setText(address);
                            addressFetchedTv.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                addressFetchedTv.setVisibility(View.GONE);
            }
        });
    }


    public void updateLocation() {
        if (!application.isConnected() || application.isOffline()) {
            ImageObject imageObject = new ImageObject(imagePath);
            imageObject.setCustomer(customer);
            new CustomerDatabase(this).saveLocationUpdate(customer, latLng, imageObject);
            Toast.makeText(application, "waiting for internet!", Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        } else {
            progressDialog.setMessage("Updating Location...");
            progressDialog.show();
            ServerRequests.getInstance(this).updateLocation(customer.getId(), latLng, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                Toast.makeText(TagLocationActivity.this, "Location updated successfully", Toast.LENGTH_SHORT).show();
                                if (!imagePath.equalsIgnoreCase("")) {
                                    ImageObject imageObject = new ImageObject(imagePath);
                                    imageObject.setCustomer(customer);
                                    Intent intent = new Intent(getApplicationContext(), ImageUploadService.class);
                                    intent.putExtra("image_object", imageObject);
                                    startService(intent);
                                }
                                setResult(RESULT_OK);
                                finish();
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Toast.makeText(TagLocationActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    error.printStackTrace();
                    Toast.makeText(TagLocationActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void setCustomerDetails() {
        searchEt.setText("");
        nameTv.setText(customer.getName());
        addressTv.setText(customer.getAddress());
        detailsLayout.setVisibility(View.VISIBLE);
        bottomLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    public void searchCustomers(String searchText, final int page) {
        if (latLng != null) {
            if (application.isConnected()) {
                application.setOffline(false);
                ServerRequests.getInstance(this).searchCustomers(searchText, latLng, page, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (response.has("message")) {
                            try {
                                if (response.getString("message").equalsIgnoreCase("success")) {
                                    if (page == 1)
                                        customers = Customer.parse(response.getJSONArray("result"));
                                    else
                                        customers.addAll(Customer.parse(response.getJSONArray("result")));
                                    adapter.setCustomers(customers);
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Toast.makeText(TagLocationActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        error.printStackTrace();
                        Toast.makeText(TagLocationActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                if (page == 1) {
                    application.setOffline(true);
                    List<Customer> allcustomers = new CustomerDatabase(this).getCustomers();
                    customers = new ArrayList<>();
                    for (Customer customer : allcustomers) {
                        if (customer.getName().toUpperCase().contains(searchText.toUpperCase())) {
                            customers.add(customer);
                        }
                    }
                    adapter.setCustomers(customers);
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraUtils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                imagePath = cameraUtils.getImagePath();
                updateLocation();
            }
        } else if (requestCode == LocationUtils.REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                LocationUtils.getSingleLocation(this, locationCallBack, enabledCallBack);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(LogoutEvent event) {
        setResult(RESULT_FIRST_USER);
        finish();
    }
}
