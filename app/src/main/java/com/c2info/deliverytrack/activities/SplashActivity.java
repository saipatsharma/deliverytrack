package com.c2info.deliverytrack.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.events.OtpReceivedEvent;
import com.c2info.deliverytrack.listeners.SuccessListener;
import com.c2info.deliverytrack.serverUtils.RequestHandler;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.services.LocationService;
import com.c2info.deliverytrack.utilities.DeliveryTrackDB;
import com.c2info.deliverytrack.utilities.KeyboardManager;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.c2info.deliverytrack.utilities.LoginPreferences;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class SplashActivity extends BaseActivity {

    private static final int PERMISSIONS_REQ_CODE = 42;
    EditText mobileNoEt, otpEt;
    TextView requestButton, loginButton, mobileTextTv, changeNumButton, mobileNumLabelTv, resendOtpButton;
    boolean timerStarted = false;
    long mobileNo;
    String otp;
    ProgressDialog progressDialog;
    LocationCallBack locationCallBack;
    LocationServicesEnabledCallBack enabledCallBack;
    MyApplication application;
    String[] permissionsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        EventBus.getDefault().register(this);

        mobileNoEt = (EditText) findViewById(R.id.mobile_no);
        requestButton = (TextView) findViewById(R.id.request);
        otpEt = (EditText) findViewById(R.id.otp);
        loginButton = (TextView) findViewById(R.id.login);
        mobileTextTv = (TextView) findViewById(R.id.mobile_no_text);
        changeNumButton = (TextView) findViewById(R.id.change_mobile_no);
        mobileNumLabelTv = (TextView) findViewById(R.id.mobile_no_label);
        resendOtpButton = (TextView) findViewById(R.id.resend_otp);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        application = (MyApplication) getApplication();

        mobileNoEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                requestOtp();
                return false;
            }
        });

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestOtp();
            }
        });

        otpEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                login();
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        changeNumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeNumber();
            }
        });

        resendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestOtp();
            }
        });

        checkForPermissions();

    }

    public void checkForLocation() {
        LocationUtils.checkAndEnable(this, new LocationServicesEnabledCallBack() {
            @Override
            public void onEnabled() {
                startTimer();
            }
        });
    }

    public void startTimer() {
        if (!timerStarted) {
            timerStarted = true;
            application.startTracking();
            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    if (LoginPreferences.getInstance(SplashActivity.this).isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        showViews();
                    }
                }
            }.start();
        }
    }

    @Subscribe
    public void onEvent(OtpReceivedEvent event) {
        if (otpEt.getVisibility() == View.VISIBLE) {
            otpEt.setText(event.getOtp());
            login();
        }
    }

    public void changeNumber() {
        KeyboardManager.hideKeyboard(this);
        mobileNoEt.setVisibility(View.VISIBLE);
        requestButton.setVisibility(View.VISIBLE);
        mobileNumLabelTv.setVisibility(View.VISIBLE);
        otpEt.setVisibility(View.GONE);
        mobileTextTv.setVisibility(View.GONE);
        loginButton.setVisibility(View.GONE);
        changeNumButton.setVisibility(View.GONE);
        resendOtpButton.setVisibility(View.GONE);
    }

    public void login() {
        KeyboardManager.hideKeyboard(this);
        if (otpEt.getText().toString().length() != 4) {
            Toast.makeText(this, "Invalid OTP", Toast.LENGTH_SHORT).show();
            return;
        }
        otp = otpEt.getText().toString();
        progressDialog.setMessage("Verifying OTP...");
        progressDialog.show();
        ServerRequests.getInstance(this).verifyOtp(mobileNo, otp, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            String token = response.getString("token");
                            boolean canTag = response.getBoolean("can_tag");
                            String c2code = response.getString("c2code");
                            LoginPreferences.getInstance(SplashActivity.this).login(mobileNo, token, canTag, c2code);
                            if (response.has("suppliers")) {
                                DeliveryTrackDB db = new DeliveryTrackDB(SplashActivity.this);
                                db.updateReasons(response.getJSONArray("suppliers"));
                            }
                            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            Toast.makeText(application, "Invalid OTP!", Toast.LENGTH_SHORT).show();
                            otpEt.setText("");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(application, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(application, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(SplashActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void requestOtp() {
        KeyboardManager.hideKeyboard(this);
        if (mobileNoEt.getText().toString().length() != 10) {
            Toast.makeText(this, "Invalid mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        mobileNo = Long.parseLong(mobileNoEt.getText().toString());
        progressDialog.setMessage("Requesting OTP...");
        progressDialog.show();
        if (application.getLatLng() != null) {
            otpServerRequest(application.getLatLng());
        } else {
            locationCallBack = new LocationCallBack() {
                @Override
                public void onLocationUpdate(Location location) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    otpServerRequest(latLng);
                }
            };
            enabledCallBack = new LocationServicesEnabledCallBack() {
                @Override
                public void onEnabled() {
                    LocationUtils.getSingleLocation(SplashActivity.this, locationCallBack, enabledCallBack);
                }
            };
            LocationUtils.getSingleLocation(this, locationCallBack, enabledCallBack);
        }
    }

    public void otpServerRequest(LatLng latLng) {
        ServerRequests.getInstance(SplashActivity.this).requestOtp(mobileNo, latLng, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            Toast.makeText(application, "OTP Sent", Toast.LENGTH_SHORT).show();
                            mobileNoEt.setVisibility(View.GONE);
                            requestButton.setVisibility(View.GONE);
                            mobileNumLabelTv.setVisibility(View.GONE);
                            otpEt.setText("");
                            otpEt.setVisibility(View.VISIBLE);
                            loginButton.setVisibility(View.VISIBLE);
                            changeNumButton.setVisibility(View.VISIBLE);
                            resendOtpButton.setVisibility(View.VISIBLE);
                            mobileTextTv.setVisibility(View.VISIBLE);
                            mobileTextTv.setText("OTP sent to " + mobileNoEt.getText().toString());
                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(SplashActivity.this, "Error in response!", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(SplashActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showViews() {
        mobileNoEt.setVisibility(View.VISIBLE);
        requestButton.setVisibility(View.VISIBLE);
        mobileNumLabelTv.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationUtils.REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                startTimer();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQ_CODE) {
            checkForPermissions();
        }
    }

    public void checkForPermissions() {
        List<String> permissions = new ArrayList<>();

        int coarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int fineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int readExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readSms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        int receiveSms = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);

        if (!(coarseLocation == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!(fineLocation == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!(camera == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.CAMERA);
        }
        if (!(readExtStorage == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!(writeExtStorage == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!(readSms == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.READ_SMS);
        }
        if (!(receiveSms == PackageManager.PERMISSION_GRANTED)) {
            permissions.add(Manifest.permission.RECEIVE_SMS);
        }

        if (permissions.size() > 0) {
            permissionsArray = new String[permissions.size()];
            permissionsArray = permissions.toArray(permissionsArray);
            new AlertDialog.Builder(this).setMessage("This app will need certain permissions to work properly. Please provide the permissions")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(SplashActivity.this, permissionsArray, PERMISSIONS_REQ_CODE);
                        }
                    }).show();
        } else {
            checkForLocation();
        }

    }

}
