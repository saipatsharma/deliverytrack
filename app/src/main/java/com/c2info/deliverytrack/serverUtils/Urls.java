package com.c2info.deliverytrack.serverUtils;

/**
 * Created by sai on 2/13/2017.
 */

public class Urls {
    private static final String serverUrl = "http://111.93.190.155/lcapi/backend/web/dt/";

    private static final String test = "?test=1";
    
    public static final String SEND_OTP = serverUrl + "send-otp" + test;
    public static final String VERIFY_OTP = serverUrl + "verify-otp/" + test;
    public static final String FETCH_DATA = serverUrl + "fetch-data/" + test;
    public static final String FETCH_DELIVERY_CUSTOMERS = serverUrl + "fetch-delivery-customers" + test;
    public static final String SEARCH_DELIVERY_CUSTOMERS = serverUrl + "search-delivery-customers/" + test;
    public static final String FETCH_INVOICES = serverUrl + "fetch-invoices/" + test;
    public static final String PAYMENT_DETAILS = serverUrl + "payment-details/" + test;
    public static final String SEARCH_CUSTOMERS = serverUrl + "search-customers/" + test;
    public static final String TAG_LOCATION = serverUrl + "tag-location/" + test;
    public static final String UPLOAD_IMAGE = serverUrl + "upload-image/" + test;
    public static final String TRACK_LOCATIONS = serverUrl + "track-location/" + test;
}
