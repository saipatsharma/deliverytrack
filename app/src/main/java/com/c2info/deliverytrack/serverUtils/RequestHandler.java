package com.c2info.deliverytrack.serverUtils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.c2info.deliverytrack.utilities.LoginPreferences;

import org.json.JSONObject;

/**
 * Created by sai on 2/13/2017.
 */

public class RequestHandler {

    private static RequestHandler requestHandler;
    private RequestQueue mRequestQueue;
    private static Context mCtx;
    private static final String SEARCH = "search-delivery-customers";

    private RequestHandler(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized RequestHandler getInstance(Context context) {
        if (requestHandler == null) {
            requestHandler = new RequestHandler(context.getApplicationContext());
        }
        return requestHandler;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) throws AuthFailureError {
        req.setRetryPolicy(new DefaultRetryPolicy(30000, 2, 2));
        getRequestQueue().add(req);
    }

    public <T> void addToSearchQueue(Request<T> req) throws AuthFailureError {
        req.setRetryPolicy(new DefaultRetryPolicy(30000, 2, 2));
        getRequestQueue().cancelAll(SEARCH);
        req.setTag(SEARCH);
        getRequestQueue().add(req);
    }

}
