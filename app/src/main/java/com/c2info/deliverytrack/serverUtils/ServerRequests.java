package com.c2info.deliverytrack.serverUtils;

import android.content.Context;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.listeners.SuccessListener;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.JSONObjectRequest;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.models.TrackedLocation;
import com.c2info.deliverytrack.utilities.LoginPreferences;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sai on 2/14/2017.
 */

public class ServerRequests {
    private String C2CODE = "LCDT01";
    private static final String IDX = "100";
    private static final int TRANS_TYPE_ACCEPTED = 1;
    private static final int TRANS_TYPE_REJECTED = 2;
    private static ServerRequests serverRequests;
    private Context context;

    private ServerRequests(Context context) {
        this.context = context;
        C2CODE = LoginPreferences.getInstance(context).getC2code();
    }

    public static synchronized ServerRequests getInstance(Context context) {
        if (serverRequests == null) {
            serverRequests = new ServerRequests(context.getApplicationContext());
        }
        return serverRequests;
    }

    public void requestOtp(long mobileNo, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("no_auth", true);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.SEND_OTP, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifyOtp(long mobileNo, String otp, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("otp", otp);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("no_auth", true);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.VERIFY_OTP, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchDeliveryCustomers(LatLng latLng, int page, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("offset", (page - 1) * 10);
            object.put("limit", 10);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.FETCH_DELIVERY_CUSTOMERS, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchDeliveryCustomers(String searchText, LatLng latLng, int page, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("cust_name", searchText);
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("offset", (page - 1) * 10);
            object.put("limit", 10);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.SEARCH_DELIVERY_CUSTOMERS, object, listener, errorListener);
            RequestHandler.getInstance(context).addToSearchQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchInvoices(LatLng latLng, String customerId, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", customerId);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.FETCH_INVOICES, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchCustomers(String searchText, LatLng latLng, int page, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("cust_name", searchText);
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("offset", (page - 1) * 10);
            object.put("limit", 10);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.SEARCH_CUSTOMERS, object, listener, errorListener);
            RequestHandler.getInstance(context).addToSearchQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateLocation(String customerId, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("customer_id", customerId);
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.TAG_LOCATION, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePayment(Customer customer, List<Invoice> invoices, Payment payment, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("customer_id", customer.getId());
            object.put("payment_method", payment.getMethod());
            object.put("amount", payment.getAmount().equalsIgnoreCase("") ? 0 : Long.parseLong(payment.getAmount()));
            object.put("reference_number", payment.getReferenceNumber());
            JSONArray accepted = new JSONArray();
            JSONArray rejected = new JSONArray();
            for (Invoice invoice : invoices) {
                if (invoice.isAccepted()) {
                    accepted.put(invoice.getInvoiceNo());
                } else if(invoice.isRejected()){
                    JSONObject reasonObj = new JSONObject();
                    reasonObj.put("invoice_no", invoice.getInvoiceNo());
                    reasonObj.put("reason_id", invoice.getRejectionReason().getId());
                    if(!invoice.getRejectionReason().getExtraReason().equalsIgnoreCase("")) {
                        reasonObj.put("reason", invoice.getRejectionReason().getExtraReason());
                    }
                    rejected.put(reasonObj);
                }
            }
            object.put("invoice_ids", accepted);
            object.put("rejection", rejected);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.PAYMENT_DETAILS, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void trackLocations(List<TrackedLocation> locations, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONArray array = new JSONArray();
            for (TrackedLocation location : locations) {
                JSONObject locObj = new JSONObject();
                locObj.put("lat", String.valueOf(location.getLatLng().latitude));
                locObj.put("lng", String.valueOf(location.getLatLng().longitude));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                locObj.put("time", dateFormat.format(location.getTime()) + " " + timeFormat.format(location.getTime()));
                array.put(locObj);
            }
            object.put("locations", array);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.TRACK_LOCATIONS, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadSignatureImage(ImageObject imageObject, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", imageObject.getCustomer().getId());
            object.put("type", "signature");
            object.put("image", imageObject.getBase64(imageObject.getSignatureImage()));
            object.put("trans_id", imageObject.getPayment().getTransId());
            object.put("trans_type", TRANS_TYPE_ACCEPTED);
            object.put("trans_det_id", "");
            object.put("filename", new File(imageObject.getSignatureImage()).getName());
            // filename and trans_det_id missing
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.UPLOAD_IMAGE, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadCustomerImage(ImageObject imageObject, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", imageObject.getCustomer().getId());
            object.put("type", "customer");
            object.put("trans_det_id", "");
            object.put("image", imageObject.getBase64(imageObject.getCustomerImage()));
            object.put("trans_id", imageObject.getPayment().getTransId());
            object.put("trans_type", TRANS_TYPE_ACCEPTED);
            object.put("filename", new File(imageObject.getCustomerImage()).getName());
            // filename and trans_det_id missing
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.UPLOAD_IMAGE, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadChequeImage(ImageObject imageObject, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", imageObject.getCustomer().getId());
            object.put("type", "cheque");
            object.put("trans_det_id", "");
            object.put("image", imageObject.getBase64(imageObject.getPayment().getImageUrl()));
            object.put("trans_id", imageObject.getPayment().getTransId());
            object.put("trans_type", TRANS_TYPE_ACCEPTED);
            object.put("filename", new File(imageObject.getPayment().getImageUrl()).getName());
            // filename and trans_det_id missing
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.UPLOAD_IMAGE, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadPrescriptionImage(ImageObject imageObject, Invoice invoice, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", imageObject.getCustomer().getId());
            object.put("type", "prescription");
            object.put("image", imageObject.getBase64(invoice.getCapturedImage()));
            object.put("trans_det_id", invoice.getTransDetId());
            object.put("trans_id", imageObject.getPayment().getTransId());
            object.put("trans_type", TRANS_TYPE_ACCEPTED);
            object.put("filename", new File(invoice.getCapturedImage()).getName());
            // filename and trans_det_id missing
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.UPLOAD_IMAGE, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadLocationImage(ImageObject imageObject, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("customer_id", imageObject.getCustomer().getId());
            object.put("type", "location");
            object.put("image", imageObject.getBase64(imageObject.getLocationImage()));
            object.put("filename", new File(imageObject.getLocationImage()).getName());
            object.put("trans_det_id","");
            object.put("trans_id", "");
            object.put("trans_type", TRANS_TYPE_ACCEPTED);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.UPLOAD_IMAGE, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchData(LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener){
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("offset", 0);
            object.put("limit", 5);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            JSONObjectRequest request = new JSONObjectRequest(context, Urls.FETCH_DATA, object, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPlace(Context context, LatLng latLng, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener){
        try {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude) + "&key=" + context.getResources().getString(R.string.google_api_key), null, listener, errorListener);
            RequestHandler.getInstance(context).addToRequestQueue(request);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
