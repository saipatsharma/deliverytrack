package com.c2info.deliverytrack.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.events.LocationEvent;
import com.c2info.deliverytrack.utilities.LocationUtils;

import org.greenrobot.eventbus.EventBus;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by sai on 2/21/2017.
 */

public class LocationService extends Service {

    LocationCallBack locationCallBack;
    LocationServicesEnabledCallBack enabledCallBack;
    private static final long REFRESH_INTERVAL = 10000L;
    private static final float REFRESH_DISTANCE = 1.0F;

    @Override
    public void onCreate() {
        super.onCreate();
        locationCallBack = new LocationCallBack() {
            @Override
            public void onLocationUpdate(Location location) {
                EventBus.getDefault().post(new LocationEvent(location));
                LocationParams params = new LocationParams.Builder().setInterval(REFRESH_INTERVAL).setAccuracy(LocationAccuracy.HIGH).setDistance(REFRESH_DISTANCE).build();
                SmartLocation.with(LocationService.this).location().config(params).start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        EventBus.getDefault().post(new LocationEvent(location));
                    }
                });
            }
        };

        enabledCallBack = new LocationServicesEnabledCallBack() {
            @Override
            public void onEnabled() {
                LocationUtils.getSingleLocation(LocationService.this, locationCallBack, enabledCallBack);
            }
        };

        LocationUtils.getSingleLocation(LocationService.this, locationCallBack, enabledCallBack);

    }

    @Override
    public void onDestroy() {
        SmartLocation.with(LocationService.this).location().stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
