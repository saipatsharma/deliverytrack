package com.c2info.deliverytrack.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.events.LocationEvent;
import com.c2info.deliverytrack.models.TrackedLocation;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.utilities.DeliveryTrackDB;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.c2info.deliverytrack.utilities.LoginPreferences;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by sai on 2/22/2017.
 */

public class TrackerService extends Service {

    LocationCallBack locationCallBack;
    LocationServicesEnabledCallBack enabledCallBack;
    private static final long REFRESH_INTERVAL = 300000L;
    private static final float REFRESH_DISTANCE = 100.0F;
    private static final long UPDATE_INTERVAL = 3600000L;
    private boolean isUpdating = false;

    @Override
    public void onCreate() {
        super.onCreate();
        locationCallBack = new LocationCallBack() {
            @Override
            public void onLocationUpdate(Location location) {
                EventBus.getDefault().post(new LocationEvent(location));
                new DeliveryTrackDB(TrackerService.this).trackLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                LocationParams params = new LocationParams.Builder().setInterval(REFRESH_INTERVAL).setAccuracy(LocationAccuracy.HIGH).setDistance(REFRESH_DISTANCE).build();
                SmartLocation.with(TrackerService.this).location().config(params).start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        EventBus.getDefault().post(new LocationEvent(location));
                        new DeliveryTrackDB(TrackerService.this).trackLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                        checkForUpdate();
                    }
                });
            }
        };

        enabledCallBack = new LocationServicesEnabledCallBack() {
            @Override
            public void onEnabled() {
                LocationUtils.getSingleLocation(TrackerService.this, locationCallBack, enabledCallBack);
            }
        };


        LocationUtils.getSingleLocation(TrackerService.this, locationCallBack, enabledCallBack);

    }

    public void checkForUpdate() {
        List<TrackedLocation> locations = new DeliveryTrackDB(TrackerService.this).getTrackedLocations();
        long currentTime = Calendar.getInstance().getTimeInMillis();
        long minimumTime = currentTime;
        for (TrackedLocation loc : locations) {
            if (loc.getTime() < minimumTime)
                minimumTime = loc.getTime();
        }
        if (currentTime - minimumTime > UPDATE_INTERVAL) {
            MyApplication application = (MyApplication) getApplication();
            if(LoginPreferences.getInstance(this).isLoggedIn() && application.isConnected()) {
                if (!isUpdating) {
                    isUpdating = true;
                    updateLocations(locations);
                }
            }
        }
    }

    public void updateLocations(final List<TrackedLocation> locations) {
        ServerRequests.getInstance(this).trackLocations(locations, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            new DeliveryTrackDB(TrackerService.this).clearLocations(locations);
                            isUpdating = false;
                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
