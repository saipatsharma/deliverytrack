package com.c2info.deliverytrack.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.utilities.CameraUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by sai on 2/22/2017.
 */

public class ImageUploadService extends IntentService {
    private Response.Listener<JSONObject> listener;
    private Response.ErrorListener errorListener;
    private MyApplication application;
    private ArrayList<ImageObject> objects;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public ImageUploadService(String name) {
        super(name);
    }

    public ImageUploadService() {
        super(ImageUploadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("ImageUploadService", "Started");
        application = (MyApplication) getApplication();
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(ImageUploadService.this, "Upload Failed!", Toast.LENGTH_SHORT).show();
                if(objects != null) {
                    processObjects();
                }
            }
        };
        ImageObject object = intent.getParcelableExtra("image_object");
        if (object != null) {
            objects = new ArrayList<>();
            objects.add(object);
            processObjects();
        } else {
            objects = intent.getParcelableArrayListExtra("image_objects");
            if(objects != null) {
                processObjects();
            }
        }
    }

    private void processObjects() {
        for (ImageObject object: objects) {
            processImages(object);
            return;
        }
        clearImages();
    }


    private void processImages(final ImageObject object) {
        if (object.getType().equalsIgnoreCase(ImageObject.Type.Location)) {
            Log.e("ImageUploadService", ImageObject.Type.Location);
            listener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                Log.e("Uploaded", "location");
                                deleteRecursive(new File(object.getLocationImage()));
                                Toast.makeText(application, "Image Uploaded!", Toast.LENGTH_SHORT).show();
                                clearImages();
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            ServerRequests.getInstance(this).uploadLocationImage(object, application.getLatLng(), listener, errorListener);
        } else {
            Log.e("ImageUploadService", ImageObject.Type.Delivery);
            processDelivery(object);
        }

    }

    private void processDelivery(final ImageObject object) {
        if (object.getCustomerImage() != null && !object.getCustomerImage().equalsIgnoreCase("")) {
            listener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                Log.e("Uploaded", "customer");
                                deleteRecursive(new File(object.getCustomerImage()));
                                object.setCustomerImage("");
                                processDelivery(object);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e("Upload Error", "customer");
                    processDelivery(object);
                }
            };
            ServerRequests.getInstance(this).uploadCustomerImage(object, application.getLatLng(), listener, errorListener);
        } else if (object.getSignatureImage() != null && !object.getSignatureImage().equalsIgnoreCase("")) {
            listener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                Log.e("Uploaded", "signature");
                                deleteRecursive(new File(object.getSignatureImage()));
                                object.setSignatureImage("");
                                processDelivery(object);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e("Upload Error", "signature");
                    processDelivery(object);
                }
            };
            ServerRequests.getInstance(this).uploadSignatureImage(object, application.getLatLng(), listener, errorListener);
        } else if (object.getPayment().getMethod().equalsIgnoreCase(Payment.Method.Cheque) && object.getPayment().getImageUrl() != null && !object.getPayment().getImageUrl().equalsIgnoreCase("")) {
            listener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.has("message")) {
                        try {
                            if (response.getString("message").equalsIgnoreCase("success")) {
                                Log.e("Uploaded", "cheque");
                                deleteRecursive(new File(object.getPayment().getImageUrl()));
                                object.getPayment().setImageUrl("");
                                processDelivery(object);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e("Upload Error", "cheque");
                    processDelivery(object);
                }
            };
            ServerRequests.getInstance(this).uploadChequeImage(object, application.getLatLng(), listener, errorListener);
        } else {
            processPrescriptions(object);
        }
    }

    private void processPrescriptions(final ImageObject object) {
        for (final Invoice invoice : object.getInvoices()) {
            if (invoice.isAccepted()) {
                if (invoice.getCapturedImage() != null && !invoice.getCapturedImage().equalsIgnoreCase("")) {
                    listener = new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.has("message")) {
                                try {
                                    if (response.getString("message").equalsIgnoreCase("success")) {
                                        Log.e("Uploaded", "prescription for " + invoice.getInvoiceNo());
                                        deleteRecursive(new File(invoice.getCapturedImage()));
                                        invoice.setCapturedImage("");
                                        processDelivery(object);
                                        return;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e("Upload Error", "prescription for " + invoice.getInvoiceNo());
                            processDelivery(object);
                        }
                    };
                    ServerRequests.getInstance(this).uploadPrescriptionImage(object, invoice, application.getLatLng(), listener, errorListener);
                    return;
                }
            }
        }
        for(ImageObject imageObject : objects) {
            if(imageObject.getCustomer().getId().equalsIgnoreCase(object.getCustomer().getId())){
                objects.remove(imageObject);
                break;
            }
        }
        processObjects();
    }

    private void clearImages(){
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), CameraUtils.IMAGE_DIRECTORY_NAME);
//        deleteRecursive(mediaStorageDir);
        Log.e("Uploaded", "Images deleted");
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }

    @Override
    public void onDestroy() {
        Log.e("ImageUploadService", "Stopped");
        super.onDestroy();
    }
}
