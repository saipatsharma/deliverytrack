package com.c2info.deliverytrack.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.events.LocationEvent;
import com.c2info.deliverytrack.models.TrackedLocation;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.DeliveryTrackDB;
import com.c2info.deliverytrack.utilities.LoginPreferences;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by sai on 3/1/2017.
 */

public class DataSyncService extends Service {

    CountDownTimer timer;
    MyApplication application;
    DeliveryTrackDB database;
    LatLng latLng = null;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        timer = new CountDownTimer(360000, 180000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                checkLocation();
            }
        };
        checkLocation();
    }

    private void checkLocation() {
        application = (MyApplication) getApplication();
        database = new DeliveryTrackDB(this);
        List<TrackedLocation> trackedLocations = database.getTrackedLocations();
        long time = 0;
        if (latLng == null) {
            latLng = application.getLatLng();
        }
        if (latLng == null) {
            if (trackedLocations.size() > 0) {
                latLng = trackedLocations.get(0).getLatLng();
                time = trackedLocations.get(0).getTime();
            }
            for (TrackedLocation location : trackedLocations) {
                if (location.getTime() > time) {
                    time = location.getTime();
                    latLng = location.getLatLng();
                }
            }
        }
        if (latLng == null) {
            timer.start();
        } else {
            MyApplication application = (MyApplication) getApplication();
            if(LoginPreferences.getInstance(this).isLoggedIn() && application.isConnected())
                fetchData();
            else
                timer.start();
        }
    }

    public void fetchData() {
        ServerRequests.getInstance(this).fetchData(latLng, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            new CustomerDatabase(DataSyncService.this).saveData(response.getJSONArray("result"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                timer.start();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                timer.start();
            }
        });
    }


    @Subscribe
    public void onEvent(LocationEvent event) {
        latLng = new LatLng(event.getLocation().getLatitude(), event.getLocation().getLongitude());
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
