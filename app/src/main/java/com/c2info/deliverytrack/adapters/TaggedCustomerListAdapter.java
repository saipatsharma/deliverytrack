package com.c2info.deliverytrack.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.CustomerSelectCallBack;
import com.c2info.deliverytrack.models.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/12/2017.
 */

public class TaggedCustomerListAdapter extends RecyclerView.Adapter<TaggedCustomerListAdapter.ViewHolder> {

    private Context context;
    private List<Customer> customers = new ArrayList<>();
    private CustomerSelectCallBack customerSelectCallBack;

    public TaggedCustomerListAdapter(Context context, CustomerSelectCallBack customerSelectCallBack) {
        this.context = context;
        this.customerSelectCallBack = customerSelectCallBack;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_tag, parent, false);
        applyFont(v);
        return new TaggedCustomerListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Customer customer = customers.get(position);
        holder.nameTv.setText(customer.getName());
        holder.addressTv.setText(customer.getAddress());
        holder.tagIndicator.setVisibility(customer.hasLocation() ? View.VISIBLE : View.GONE);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerSelectCallBack.onSelect(customer);
            }
        });
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv, addressTv;
        LinearLayout tagIndicator;
        View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.name);
            tagIndicator = (LinearLayout) itemView.findViewById(R.id.tag_indicator);
            addressTv = (TextView) itemView.findViewById(R.id.address);
            rootView = itemView;
        }
    }

    private void applyFont(final View root) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(viewGroup.getChildAt(i));
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getApplicationContext().getAssets(), context.getApplicationContext().getString(R.string.font)));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", "LATO", root));
            e.printStackTrace();
        }
    }
}
