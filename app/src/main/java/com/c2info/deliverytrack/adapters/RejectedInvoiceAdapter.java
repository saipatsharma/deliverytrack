package com.c2info.deliverytrack.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.RejectedInvoiceCheckCallBack;
import com.c2info.deliverytrack.models.Invoice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/15/2017.
 */

public class RejectedInvoiceAdapter extends RecyclerView.Adapter<RejectedInvoiceAdapter.ViewHolder> {

    Context context;
    List<Invoice> invoices = new ArrayList<>();
    RejectedInvoiceCheckCallBack callBack;
    boolean refresh = false;
    List<ViewHolder> viewHolders = new ArrayList<>();
    String space = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRs.";

    public RejectedInvoiceAdapter(Context context, RejectedInvoiceCheckCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
        refresh = true;
        notifyDataSetChanged();
        refresh = false;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rejected_invoice, parent, false);
        applyFont(v);
        ViewHolder holder = new RejectedInvoiceAdapter.ViewHolder(v);
        viewHolders.add(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Invoice invoice = invoices.get(position);
        holder.checkBox.setText(invoice.getInvoiceNo() + space + invoice.getAmount());
        holder.checkBox.setChecked(invoice.isRejectionChecked());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!refresh)
                    callBack.onCheckChanged(invoice, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView;
        }
    }

    private void applyFont(final View root) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(viewGroup.getChildAt(i));
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getApplicationContext().getAssets(), context.getApplicationContext().getString(R.string.font)));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", "LATO", root));
            e.printStackTrace();
        }
    }
}
