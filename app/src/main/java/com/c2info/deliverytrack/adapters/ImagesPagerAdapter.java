package com.c2info.deliverytrack.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.NavigationCallback;
import com.c2info.deliverytrack.fragments.ImageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/14/2017.
 */

public class ImagesPagerAdapter extends FragmentPagerAdapter {

    private List<String> imageUrls = new ArrayList<>();
    private Context context;
    private FragmentManager fragmentManager;
    private NavigationCallback navigationCallback;

    public ImagesPagerAdapter(FragmentManager fm, Context context, List<String> imageUrls, NavigationCallback navigationCallback) {
        super(fm);
        fragmentManager = fm;
        this.context = context;
        this.imageUrls = imageUrls;
        this.navigationCallback = navigationCallback;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.setImage(context, imageUrls.get(position), position, imageUrls.size(), navigationCallback);
        return imageFragment;
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

}
