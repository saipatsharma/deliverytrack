package com.c2info.deliverytrack.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.InvoiceCheckCallBack;
import com.c2info.deliverytrack.models.Invoice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/9/2017.
 */

public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Invoice> invoices = new ArrayList<>();
    private InvoiceCheckCallBack callBack;

    public InvoiceListAdapter(Context context, InvoiceCheckCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    public void setInvoices(ArrayList<Invoice> invoices, boolean notify) {
        this.invoices = invoices;
        if(notify)
            notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoice, parent, false);
        applyFont(v);
        return new InvoiceListAdapter.ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Invoice invoice = invoices.get(position);
        if(holder.invoice == null) {
            holder.invoice = invoice;
        }
        holder.invoiceNoTv.setText(holder.invoice.getInvoiceNo());
        holder.amountTv.setText("Rs." + holder.invoice.getAmount());
        holder.checkBox.setChecked(holder.invoice.isAccepted());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.invoice.setAccepted(isChecked);
                callBack.onCheckChanged(invoices);
            }
        });
        holder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setChecked(false);
                holder.invoice.setAccepted(false);
                callBack.onReject(invoice);
            }
        });
        holder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onClick(invoice);
            }
        });
        if(invoice.isRejected()) {
            holder.checkBox.setVisibility(View.INVISIBLE);
            holder.rejectButton.setVisibility(View.INVISIBLE);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.rejectButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView invoiceNoTv, amountTv;
        CheckBox checkBox;
        ImageView rejectButton;
        View rootView;
        LinearLayout clickLayout;
        Invoice invoice;

        public ViewHolder(View itemView) {
            super(itemView);
            invoiceNoTv = (TextView) itemView.findViewById(R.id.invoice_no);
            amountTv = (TextView) itemView.findViewById(R.id.amount);
            checkBox = (CheckBox) itemView.findViewById(R.id.check);
            rejectButton = (ImageView) itemView.findViewById(R.id.reject);
            clickLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            rootView = itemView;
        }
    }

    private void applyFont(final View root) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(viewGroup.getChildAt(i));
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getApplicationContext().getAssets(), context.getApplicationContext().getString(R.string.font)));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", "LATO", root));
            e.printStackTrace();
        }
    }
}
