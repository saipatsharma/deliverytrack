package com.c2info.deliverytrack.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.callbacks.PackageCheckCallBack;
import com.c2info.deliverytrack.models.Package;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/15/2017.
 */

public class PackageListAdapter extends RecyclerView.Adapter<PackageListAdapter.ViewHolder> {
    Context context;
    List<Package> packages = new ArrayList<>();
    PackageCheckCallBack callBack;

    public PackageListAdapter(Context context, PackageCheckCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_package, parent, false);
        applyFont(v);
        return new PackageListAdapter.ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Package packageInfo = packages.get(position);
        holder.itemTv.setText(packageInfo.getItem().toUpperCase());
        holder.quantityTv.setText(String.valueOf(packageInfo.getQuantity()));
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                packageInfo.setChecked(isChecked);
                callBack.onCheckChanged(packages);
            }
        });
    }

    @Override
    public int getItemCount() {
        return packages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTv, quantityTv;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTv = (TextView) itemView.findViewById(R.id.item);
            quantityTv = (TextView) itemView.findViewById(R.id.quantity);
            checkBox = (CheckBox) itemView.findViewById(R.id.check);
        }
    }

    private void applyFont(final View root) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(viewGroup.getChildAt(i));
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getApplicationContext().getAssets(), context.getApplicationContext().getString(R.string.font)));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", "LATO", root));
            e.printStackTrace();
        }
    }
}
