package com.c2info.deliverytrack.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.c2info.deliverytrack.MyApplication;
import com.c2info.deliverytrack.R;
import com.c2info.deliverytrack.activities.InvoiceListActivity;
import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.serverUtils.ServerRequests;
import com.c2info.deliverytrack.utilities.CustomerDatabase;
import com.c2info.deliverytrack.utilities.LocationUtils;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/9/2017.
 */

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.ViewHolder> {

    public static final int INVOICE_LIST_REQ_CODE = 423;
    private Context context;
    private List<Customer> customers = new ArrayList<>();
    private LocationCallBack locationCallBack;
    private LocationServicesEnabledCallBack enabledCallBack;
    private ProgressDialog progressDialog;
    private MyApplication application;

    public CustomerListAdapter(Context context) {
        this.context = context;
        application = (MyApplication) ((Activity) context).getApplication();
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer, parent, false);
        applyFont(v);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Customer customer = customers.get(position);
        holder.nameTv.setText(customer.getName());
        holder.addressTv.setText("Address: " + customer.getAddress());
        holder.pendingTv.setText(String.valueOf(customer.getPendingInvoices()));
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customer.getPendingInvoices() > 0) {
                    fetchInvoices(customer);
                } else {
                    Toast.makeText(context, "This customer do not have any pending invoices!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void fetchInvoices(final Customer customer) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Fetching Invoices...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        if(!application.isOffline()) {
            if (application.getLatLng() != null) {
                fetchInvoiceRequest(application.getLatLng(), customer);
            } else {
                locationCallBack = new LocationCallBack() {
                    @Override
                    public void onLocationUpdate(Location location) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        fetchInvoiceRequest(latLng, customer);
                    }
                };
                enabledCallBack = new LocationServicesEnabledCallBack() {
                    @Override
                    public void onEnabled() {
                        LocationUtils.getSingleLocation(context, locationCallBack, enabledCallBack);
                    }
                };
                LocationUtils.getSingleLocation(context, locationCallBack, enabledCallBack);
            }
        } else {
            CustomerDatabase database = new CustomerDatabase(context);
            ArrayList<Invoice> invoices = database.getInvoices(customer.getId());
            ArrayList<String> paymentMethods = database.getPaymentMethods(customer.getId());
            openInvoices(customer, invoices, paymentMethods);
            if(progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    private void fetchInvoiceRequest(LatLng latLng, final Customer customer) {
        ServerRequests.getInstance(context).fetchInvoices(latLng, customer.getId(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Log.e("response", response.toString());
                if (response.has("message")) {
                    try {
                        if (response.getString("message").equalsIgnoreCase("success")) {
                            ArrayList<Invoice> invoices = Invoice.parse(response.getJSONArray("result"));
                            ArrayList<String> paymentOptions = new ArrayList<String>();
                            for (int i = 0; i < response.getJSONArray("payment_methods").length(); i++) {
                                paymentOptions.add(response.getJSONArray("payment_methods").getString(i));
                            }
                            openInvoices(customer, invoices, paymentOptions);
                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(context, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openInvoices(Customer customer, ArrayList<Invoice> invoices, ArrayList<String> paymentOptions) {
        Intent intent = new Intent(context, InvoiceListActivity.class);
        intent.putExtra("invoices", invoices);
        intent.putExtra("customer", customer);
        intent.putExtra("payment_methods", paymentOptions);
        ((Activity) context).startActivityForResult(intent, INVOICE_LIST_REQ_CODE);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv, pendingTv, addressTv;
        View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.name);
            pendingTv = (TextView) itemView.findViewById(R.id.pending);
            addressTv = (TextView) itemView.findViewById(R.id.address);
            rootView = itemView;
        }
    }

    private void applyFont(final View root) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(viewGroup.getChildAt(i));
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getApplicationContext().getAssets(), context.getApplicationContext().getString(R.string.font)));
        } catch (Exception e) {
            Log.e("ProjectName", String.format("Error occured when trying to apply %s font for %s view", "LATO", root));
            e.printStackTrace();
        }
    }
}
