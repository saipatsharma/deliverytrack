package com.c2info.deliverytrack.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;

import com.c2info.deliverytrack.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sai on 2/12/2017.
 */

public class CameraUtils {
    private static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri;
    public static final String IMAGE_DIRECTORY_NAME = ".dtfiles";
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private String imagePath = "";
    private Activity activity;
    private Bitmap resultImage;

    public static CameraUtils getInstance(Activity activity) {
        return new CameraUtils(activity);
    }

    private CameraUtils(Activity activity) {
        this.activity = activity;
    }

    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        imagePath = fileUri.getPath();
        activity.startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private Uri getOutputMediaFileUri(int type) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N){
            return FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(type));
        } else{
            return Uri.fromFile(getOutputMediaFile(type));
        }
    }

    private File getOutputMediaFile(int type) {
        File mediaFile;
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + ".IMG_" + timeStamp + ".jpg");
            try {
                mediaFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            return null;
        }
        return mediaFile;
    }

    public String getImagePath() {
        getBitmap();
        return imagePath;
    }

    public Bitmap getBitmap() {
        resultImage = decodeSampledBitmapFromResource(imagePath);
        long originalSize = new File(imagePath).length();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(imagePath);
            resultImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultImage;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        int reqSize = 800;
        int reqWidth = width, reqHeight = height;
        if (height > reqSize || width > reqSize) {
            int maxSize = Math.max(height, width);
            int factor = maxSize / reqSize;
            reqWidth = width / factor;
            reqHeight = height / factor;
        }
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeSampledBitmapFromResource(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }
}
