package com.c2info.deliverytrack.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.c2info.deliverytrack.models.Reason;
import com.c2info.deliverytrack.models.TrackedLocation;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sai on 2/21/2017.
 */

public class DeliveryTrackDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "deliverytrack";
    private static final int DB_VERSION = 1;

    private static final String REASON_TABLE = "reasons";
    private static final String REASON = "reason";
    private static final String REASON_ID = "reason_id";
    private static final String NAME = "name";

    private static final String LOCATION_TABLE = "locations";
    private static final String LAT = "lat";
    private static final String LON = "lon";
    private static final String TIME = "time";

    public DeliveryTrackDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + REASON_TABLE + " (" + NAME + " varchar(100)," + REASON + " varchar(500)," + REASON_ID + " varchar(50));");
        db.execSQL("create table " + LOCATION_TABLE + " (" + LAT + " varchar(50)," + LON + " varchar(50)," + TIME + " varchar(70));");

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + REASON_TABLE);
        db.execSQL("drop table if exists " + LOCATION_TABLE);
        onCreate(db);
    }

    public void trackLocation(LatLng latLng) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LAT, String.valueOf(latLng.latitude));
        contentValues.put(LON, String.valueOf(latLng.longitude));
        contentValues.put(TIME, String.valueOf(Calendar.getInstance().getTimeInMillis()));
        database.insert(LOCATION_TABLE, null, contentValues);
        database.close();
    }

    public List<TrackedLocation> getTrackedLocations() {
        SQLiteDatabase database = getReadableDatabase();
        String[] columns = {LAT, LON, TIME};
        Cursor cr = database.query(LOCATION_TABLE, columns, null, null, null, null, null);
        List<TrackedLocation> locations = new ArrayList<>();
        while (cr.moveToNext()) {
            double lat = Double.valueOf(cr.getString(0));
            double lon = Double.valueOf(cr.getString(1));
            long time = Long.parseLong(cr.getString(2));
            locations.add(new TrackedLocation(new LatLng(lat, lon), time));
        }
        database.close();
        return locations;
    }

    public void clearLocations(List<TrackedLocation> locations){
        SQLiteDatabase database = getWritableDatabase();
        for(TrackedLocation location : locations) {
            String[] where = {String.valueOf(location.getTime())};
            database.delete(LOCATION_TABLE, TIME + " = ?", where);
        }
        database.close();
    }

    public void updateReasons(JSONArray array) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.delete(REASON_TABLE, null, null);
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONArray reasonArray = array.getJSONObject(i).getJSONArray("rejection_reasons");
                for (int j = 0; j < reasonArray.length(); j++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(NAME, array.getJSONObject(i).getString("supplier_name"));
                    contentValues.put(REASON, reasonArray.getJSONObject(j).getString("reason"));
                    contentValues.put(REASON_ID, reasonArray.getJSONObject(j).getString("reason_id"));
                    sqLiteDatabase.insert(REASON_TABLE, null, contentValues);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        sqLiteDatabase.close();
    }

    public ArrayList<Reason> getReasons(String supplierName) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String[] columns = {NAME, REASON, REASON_ID};
        String[] whereStr = {supplierName};
        Cursor cr = sqLiteDatabase.query(REASON_TABLE, columns, NAME + "= ?", whereStr, null, null, null);
        ArrayList<Reason> myList = new ArrayList<>();
        while (cr.moveToNext()) {
            String name = cr.getString(0);
            String reason = cr.getString(1);
            String reasonId = cr.getString(2);
            if (name.equalsIgnoreCase(supplierName)) {
                Reason reasonObj = new Reason(reasonId, reason);
                myList.add(reasonObj);
            }
        }
        sqLiteDatabase.close();
        return myList;
    }
}
