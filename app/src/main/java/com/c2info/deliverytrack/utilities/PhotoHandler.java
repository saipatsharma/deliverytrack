package com.c2info.deliverytrack.utilities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.c2info.deliverytrack.BuildConfig;
import com.c2info.deliverytrack.callbacks.PhotoCapturedCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sai on 2/23/2017.
 */

public class PhotoHandler implements Camera.PictureCallback {

    private final Context context;
    private String filePath = "";
    private int cameraId = -1;
    private PhotoCapturedCallback capturedCallback;

    public PhotoHandler(Context context, PhotoCapturedCallback capturedCallback) {
        this.context = context;
        this.capturedCallback = capturedCallback;
    }

    public int getCameraId(){
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            cameraId = findRearFacingCamera();
        } else {
            cameraId = findFrontFacingCamera();
        }
        return cameraId;
    }

    private int findRearFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File pictureFile = getOutputMediaFile();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N){
                filePath = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", pictureFile).getPath();
            } else{
                filePath = Uri.fromFile(pictureFile).getPath();
            }
            capturedCallback.onCapture(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getOutputMediaFile() {
        File mediaFile;
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), CameraUtils.IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + ".IMG_" + timeStamp + ".jpg");
        try {
            mediaFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mediaFile;
    }
}
