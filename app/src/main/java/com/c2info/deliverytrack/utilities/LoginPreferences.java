package com.c2info.deliverytrack.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sai on 2/8/2017.
 */

public class LoginPreferences {

    private static final String USERTABLE = "usertable";

    private static final String IS_LOGGED_IN = "isLoggedIn";

    private static final String MOBILE_NO = "mobile_no";
    private static final String TOKEN = "token";
    private static final String LAST_LOGIN_TIME = "last_login_time";
    private static final String CAN_TAG = "can_tag";
    private static final String C2CODE = "c2code";

    private static final long REFRESH_RATE = 2 * 60 * 60 * 1000;

    private SharedPreferences preferences;
    private static LoginPreferences loginPreferences;
    private Context context;

    public static synchronized LoginPreferences getInstance(Context context) {
        if (loginPreferences == null) {
            loginPreferences = new LoginPreferences(context.getApplicationContext());
        }
        return loginPreferences;
    }

    private LoginPreferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(USERTABLE, Context.MODE_PRIVATE);
    }

    public boolean isLoggedIn() {
        if (preferences.getBoolean(IS_LOGGED_IN, false)) {
            long now = Calendar.getInstance().getTimeInMillis();
            long lastLoginTime = preferences.getLong(LAST_LOGIN_TIME, 0);
            if ((now - lastLoginTime) > REFRESH_RATE) {
                logout();
            }
        }
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void login(long mobileNo, String token, boolean canTag, String c2code) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(MOBILE_NO, mobileNo);
        editor.putString(TOKEN, token);
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putBoolean(CAN_TAG, canTag);
        editor.putString(C2CODE, c2code);
        editor.putLong(LAST_LOGIN_TIME, Calendar.getInstance().getTimeInMillis());
        editor.apply();
    }

    public String getC2code() {
        return preferences.getString(C2CODE, "LCDT01");
    }

    public boolean canTag() {
        return preferences.getBoolean(CAN_TAG, false);
    }

    public long getMobileNo() {
        return preferences.getLong(MOBILE_NO, 0);
    }

    public void extendTime() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(LAST_LOGIN_TIME, Calendar.getInstance().getTimeInMillis());
        editor.apply();
    }

    public void logout() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        editor.putBoolean(IS_LOGGED_IN, false);
        editor.apply();
        DeliveryTrackDB trackDB = new DeliveryTrackDB(context);
        trackDB.clearLocations(trackDB.getTrackedLocations());
        new CustomerDatabase(context).clearData();
    }

    public String getToken() {
        if (preferences.getBoolean(IS_LOGGED_IN, false)) {
            return preferences.getString(TOKEN, "");
        }
        return "";
    }

    public String getAuthToken() {
        if (preferences.getBoolean(IS_LOGGED_IN, false)) {
            String token = preferences.getString(TOKEN, "");
            String mobileNo = String.valueOf(preferences.getLong(MOBILE_NO, 0));
            String deviceid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//            String mobileNo = "9999999999";
//            String deviceid = "deviceid";
            return token + mobileNo + deviceid;
        }
        return "";
    }

    public String getKey() {
        try {
            String token = "";
            token = getAuthToken();
            MessageDigest digest = null;
            digest = MessageDigest.getInstance("SHA-256");
            byte[] key = digest.digest(token.getBytes());
            String keyStr = bin2hex(key).toLowerCase();
            digest = MessageDigest.getInstance("MD5");
            key = digest.digest(keyStr.getBytes());
            keyStr = bin2hex(key).toLowerCase();
            if (keyStr.length() > 16) {
                return keyStr.substring(0, 16);
            } else {
                return keyStr;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
    }

    public String encrypt(String value) {
        try {
            byte[] key = getKey().getBytes();

            String initVector = "xxxxyyyyzzzzwwww";

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes());
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            return Base64.encodeToString(encrypted, Base64.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private String decrypt(String encrypted) {
        try {
            String token = getAuthToken();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] key = digest.digest(token.getBytes());
            digest = MessageDigest.getInstance("MD5");
            key = digest.digest(key);

            String initVector = "xxxxyyyyzzzzwwww";

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes());
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
