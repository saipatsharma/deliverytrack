package com.c2info.deliverytrack.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.c2info.deliverytrack.callbacks.LocationCallBack;
import com.c2info.deliverytrack.callbacks.LocationServicesEnabledCallBack;
import com.c2info.deliverytrack.listeners.DTLocationListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.LocationProvider;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import io.nlopez.smartlocation.location.providers.LocationManagerProvider;

import static io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider.REQUEST_CHECK_SETTINGS;

/**
 * Created by sai on 2/16/2017.
 */

public class LocationUtils {

    public static final int LOCATION_SERVICES_DISABLED = 1;
    public static final int ENABLE_GPS_REQUEST = 3;
    public static final int REQUEST_CHECK_SETTINGS = 43;

    public static boolean isGPSProviderEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetworkProviderEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isLocationServicesEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            int locationProviders1 = 0;
            try {
                locationProviders1 = Settings.Secure.getInt(context.getContentResolver(), "location_mode");
            } catch (Settings.SettingNotFoundException var3) {
                var3.printStackTrace();
            }
            return locationProviders1 != 0;
        } else {
            String locationProviders = Settings.Secure.getString(context.getContentResolver(), "location_providers_allowed");
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static void getSingleLocation(Context context, LocationCallBack callBack, LocationServicesEnabledCallBack enabledCallBack) {
        if (!isLocationServicesEnabled(context)) {
            requestEnableLocationServices(context, enabledCallBack);
            return;
        }
        if (!isGPSProviderEnabled(context) && !isNetworkProviderEnabled(context)) {
            requestEnableLocationServices(context, enabledCallBack);
            return;
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        DTLocationListener listener = new DTLocationListener(callBack);
        if (isNetworkProviderEnabled(context)) {
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, listener, null);
        } else if (isGPSProviderEnabled(context)) {
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, listener, null);
        } else {
            requestEnableLocationServices(context, enabledCallBack);
        }
    }

    public static void getLocationUpdates(Context context, LocationCallBack callBack, LocationServicesEnabledCallBack enabledCallBack) {
        if (!isLocationServicesEnabled(context)) {
            requestEnableLocationServices(context, enabledCallBack);
            return;
        }
        if (!isGPSProviderEnabled(context) && !isNetworkProviderEnabled(context)) {
            requestEnableLocationServices(context, enabledCallBack);
            return;
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        DTLocationListener listener = new DTLocationListener(callBack);
        if (isNetworkProviderEnabled(context)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 1.0F, listener);
        } else if (isGPSProviderEnabled(context)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 1.0F, listener);
        } else {
            requestEnableLocationServices(context, enabledCallBack);
        }
    }


    public static void checkAndEnable(Context context, LocationServicesEnabledCallBack callBack) {
        if((!isLocationServicesEnabled(context)) || (!isGPSProviderEnabled(context) && !isNetworkProviderEnabled(context))) {
            requestEnableLocationServices(context, callBack);
        } else {
            callBack.onEnabled();
        }
    }

    private static void requestEnableLocationServices(final Context context, final LocationServicesEnabledCallBack callBack) {
        GoogleApiClient apiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();
        apiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(apiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        callBack.onEnabled();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult((Activity) context, REQUEST_CHECK_SETTINGS);
                        } catch (Exception e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }



}
