package com.c2info.deliverytrack.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import com.c2info.deliverytrack.models.CompletedRequest;
import com.c2info.deliverytrack.models.Customer;
import com.c2info.deliverytrack.models.ImageObject;
import com.c2info.deliverytrack.models.Invoice;
import com.c2info.deliverytrack.models.Package;
import com.c2info.deliverytrack.models.Payment;
import com.c2info.deliverytrack.models.TrackedLocation;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sai on 3/2/2017.
 */

public class CustomerDatabase extends SQLiteOpenHelper {

    private Context context;
    private static final String C2CODE = "LCDT01";
    private static final String IDX = "100";

    private static final String DB_NAME = "dtcustomers";
    private static final int DB_VERSION = 1;

    private static final String CUSTOMER_TABLE = "customer_table";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String CUSTOMER_LAT = "lat";
    private static final String CUSTOMER_LNG = "lng";
    private static final String CUSTOMER_NAME = "customer_name";
    private static final String CUSTOMER_ADDRESS = "address";
    private static final String CUSTOMER_PENDING = "pending_invoice";
    private static final String CUSTOMER_SUPPLIER = "supplier_name";

    private static final String INVOICE_TABLE = "invoice_table";
    private static final String INVOICE_ID = "invoice_id";
    private static final String INVOICE_AMOUNT = "amount";
    private static final String INVOICE_PACKAGE = "package_info";
    private static final String INVOICE_PRESCRIPTION = "prescription";

    private static final String PACKAGE_TABLE = "package_table";
    private static final String PACKAGE_ITEM = "item";
    private static final String PACKAGE_QUANTITY = "quantity";

    private static final String PRESCRIPTION_TABLE = "prescription_table";
    private static final String PRESCRIPTION_URL = "image_url";

    private static final String PAYMENT_TABLE = "payment_table";
    private static final String PAYMENT_METHOD = "method";

    private static final String COMPLETED_REQUEST_TABLE = "completed_table";
    private static final String REQUEST = "request";
    private static final String IMAGES = "images";
    private static final String TIME = "time";
    private static final String TYPE = "type";

    private static final String DELIVERY = "delivery";
    private static final String LOCATION = "location";

    public CustomerDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + CUSTOMER_TABLE + " (" + CUSTOMER_ID + " varchar(100),"
                + CUSTOMER_LAT + " varchar(50)," + CUSTOMER_LNG + " varchar(50),"
                + CUSTOMER_NAME + " varchar(100)," + CUSTOMER_ADDRESS + " varchar(200),"
                + CUSTOMER_PENDING + " varchar(50)," + CUSTOMER_SUPPLIER + " varchar(100)"
                + ");");
        db.execSQL("create table " + INVOICE_TABLE + " (" + INVOICE_ID + " varchar(100),"
                + INVOICE_AMOUNT + " varchar(50)," + INVOICE_PACKAGE + " varchar(50),"
                + INVOICE_PRESCRIPTION + " varchar(100)," + CUSTOMER_ID + " varchar(100)"
                + ");");
        db.execSQL("create table " + PACKAGE_TABLE + " (" + INVOICE_ID + " varchar(100),"
                + PACKAGE_ITEM + " varchar(100)," + PACKAGE_QUANTITY + " varchar(50)"
                + ");");
        db.execSQL("create table " + PRESCRIPTION_TABLE + " (" + INVOICE_ID + " varchar(100),"
                + PRESCRIPTION_URL + " varchar(200)" + ");");
        db.execSQL("create table " + PAYMENT_TABLE + " (" + CUSTOMER_ID + " varchar(100),"
                + PAYMENT_METHOD + " varchar(100)" + ");");
        db.execSQL("create table " + COMPLETED_REQUEST_TABLE + " (" + REQUEST + " varchar(20000),"
                + IMAGES + " varchar(20000)," + TIME + " varchar(200)," + TYPE + " varchar(50)" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + CUSTOMER_TABLE);
        db.execSQL("drop table if exists " + INVOICE_TABLE);
        db.execSQL("drop table if exists " + PACKAGE_TABLE);
        db.execSQL("drop table if exists " + PRESCRIPTION_TABLE);
        db.execSQL("drop table if exists " + PAYMENT_TABLE);
        db.execSQL("drop table if exists " + COMPLETED_REQUEST_TABLE);
        onCreate(db);
    }

    public ArrayList<CompletedRequest> getCompletedDeliveryRequests() {
        ArrayList<CompletedRequest> requests = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        String[] columns = {REQUEST, IMAGES, TIME};
        String[] where = {DELIVERY};
        Cursor cursor = database.query(COMPLETED_REQUEST_TABLE, columns, TYPE + " = ?", where, null, null, null);
        while (cursor.moveToNext()) {
            String request = cursor.getString(0);
            String images = cursor.getString(1);
            String time = cursor.getString(2);
            requests.add(new CompletedRequest(request, images, time));
        }
        database.close();
        return requests;
    }

    public ArrayList<CompletedRequest> getLocationRequests() {
        ArrayList<CompletedRequest> requests = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        String[] columns = {REQUEST, IMAGES, TIME};
        String[] where = {LOCATION};
        Cursor cursor = database.query(COMPLETED_REQUEST_TABLE, columns, TYPE + " = ?", where, null, null, null);
        while (cursor.moveToNext()) {
            String request = cursor.getString(0);
            String images = cursor.getString(1);
            String time = cursor.getString(2);
            requests.add(new CompletedRequest(request, images, time));
        }
        database.close();
        return requests;
    }

    public void clearRequest(String time) {
        SQLiteDatabase database = getWritableDatabase();
        String[] where = {time};
        database.delete(COMPLETED_REQUEST_TABLE, TIME + " = ?", where);
        database.close();
    }

    public void saveLocationUpdate(Customer customer, LatLng latLng, ImageObject imageObject) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("customer_id", customer.getId());
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            ContentValues contentValues = new ContentValues();
            contentValues.put(REQUEST, object.toString());
            contentValues.put(IMAGES, imageObject.getImagesString());
            contentValues.put(TIME, String.valueOf(Calendar.getInstance().getTimeInMillis()));
            contentValues.put(TYPE, LOCATION);
            database.insert(COMPLETED_REQUEST_TABLE, null, contentValues);
            ContentValues contentValues1 = new ContentValues();
            contentValues1.put(CUSTOMER_LAT, String.valueOf(latLng.latitude));
            contentValues1.put(CUSTOMER_LNG, String.valueOf(latLng.longitude));
            String[] where = {customer.getId()};
            database.update(CUSTOMER_TABLE, contentValues1, CUSTOMER_ID + " = ?", where);
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }

    public void saveCompletedDelivery(Customer customer, ArrayList<Invoice> invoices, Payment payment, LatLng latLng, ImageObject imageObject) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            long mobileNo = LoginPreferences.getInstance(context).getMobileNo();
            JSONObject object = new JSONObject();
            object.put("mobile_no", mobileNo);
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            object.put("c2code", C2CODE);
            object.put("idx", IDX);
            object.put("customer_id", customer.getId());
            object.put("payment_method", payment.getMethod());
            object.put("amount", payment.getAmount().equalsIgnoreCase("") ? 0 : Long.parseLong(payment.getAmount()));
            object.put("reference_number", payment.getReferenceNumber());
            JSONArray accepted = new JSONArray();
            JSONArray rejected = new JSONArray();
            ArrayList<String> acceptedInvoices = new ArrayList<>();
            for (Invoice invoice : invoices) {
                if (invoice.isAccepted()) {
                    accepted.put(invoice.getInvoiceNo());
                    acceptedInvoices.add(invoice.getInvoiceNo());
                } else {
                    JSONObject reasonObj = new JSONObject();
                    reasonObj.put("invoice_no", invoice.getInvoiceNo());
                    reasonObj.put("reason_id", invoice.getRejectionReason().getId());
                    if(!invoice.getRejectionReason().getExtraReason().equalsIgnoreCase("")) {
                        reasonObj.put("reason", invoice.getRejectionReason().getExtraReason());
                    }
                    rejected.put(reasonObj);
                }
            }
            object.put("invoice_ids", accepted);
            object.put("rejection", rejected);
            ContentValues contentValues = new ContentValues();
            contentValues.put(REQUEST, object.toString());
            contentValues.put(IMAGES, imageObject.getImagesString());
            contentValues.put(TIME, String.valueOf(Calendar.getInstance().getTimeInMillis()));
            contentValues.put(TYPE, DELIVERY);
            database.insert(COMPLETED_REQUEST_TABLE, null, contentValues);
            for(String invoiceId: acceptedInvoices) {
                String[] where = {invoiceId};
                database.delete(INVOICE_TABLE, INVOICE_ID + " = ?", where);
                database.delete(PACKAGE_TABLE, INVOICE_ID + " = ?", where);
                database.delete(PRESCRIPTION_TABLE, INVOICE_ID + " = ?", where);
            }
            String[] where = {customer.getId()};
            if(acceptedInvoices.size() == invoices.size()) {
                database.delete(CUSTOMER_TABLE, CUSTOMER_ID + " = ?", where);
            } else {
                ContentValues cv = new ContentValues();
                cv.put(CUSTOMER_PENDING, invoices.size() - acceptedInvoices.size());
                database.update(CUSTOMER_TABLE, cv, CUSTOMER_ID + " = ?", where);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }

    public List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        String[] columns = {CUSTOMER_ID, CUSTOMER_LAT, CUSTOMER_LNG, CUSTOMER_NAME, CUSTOMER_ADDRESS, CUSTOMER_PENDING, CUSTOMER_SUPPLIER};
        Cursor cr = database.query(CUSTOMER_TABLE, columns, null, null, null, null, null);
        while (cr.moveToNext()) {
            String id = cr.getString(0);
            String name = cr.getString(3);
            String address = cr.getString(4);
            int pending = Integer.parseInt(cr.getString(5));
            String supplierName = cr.getString(6);
            Customer customer = new Customer(id, name, address, pending, false, supplierName);
            if (!cr.getString(1).equalsIgnoreCase("") && !cr.getString(2).equalsIgnoreCase("")) {
                customer.setHasLocation(true);
            }
            customers.add(customer);
        }
        database.close();
        return customers;
    }

    public ArrayList<Invoice> getInvoices(String customerID) {
        ArrayList<Invoice> invoices = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        String[] columns = {INVOICE_ID, INVOICE_AMOUNT, INVOICE_PACKAGE, INVOICE_PRESCRIPTION};
        String[] where = {customerID};
        Cursor cr = database.query(INVOICE_TABLE, columns, CUSTOMER_ID + " = ?", where, null, null, null);
        while (cr.moveToNext()) {
            String invoiceId = cr.getString(0);
            double amount = Double.parseDouble(cr.getString(1));
            boolean hasPackage = Boolean.parseBoolean(cr.getString(2));
            boolean hasPrescription = Boolean.parseBoolean(cr.getString(3));
            List<Package> packages = new ArrayList<>();
            List<String> prescriptionUrls = new ArrayList<>();

            String[] whereArray = {invoiceId};

            String[] packageColumns = {PACKAGE_ITEM, PACKAGE_QUANTITY};
            Cursor packageCR = database.query(PACKAGE_TABLE, packageColumns, INVOICE_ID + " = ?", whereArray, null, null, null);
            while (packageCR.moveToNext()) {
                String item = packageCR.getString(0);
                double quantity = Double.parseDouble(packageCR.getString(1));
                packages.add(new Package(item, quantity));
            }

            String[] prescriptionColumns = {PRESCRIPTION_URL};
            Cursor prescriptionCR = database.query(PRESCRIPTION_TABLE, prescriptionColumns, INVOICE_ID + " = ?", whereArray, null, null, null);
            while (prescriptionCR.moveToNext()) {
                prescriptionUrls.add(prescriptionCR.getString(0));
            }

            invoices.add(new Invoice(invoiceId, amount, hasPackage, hasPrescription, prescriptionUrls, packages));
        }
        database.close();
        return invoices;
    }

    public ArrayList<String> getPaymentMethods(String customerId) {
        ArrayList<String> paymentMethods = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        String[] paymentColumns = {PAYMENT_METHOD};
        String[] whereArray = {customerId};
        Cursor paymentCR = database.query(PAYMENT_TABLE, paymentColumns, CUSTOMER_ID + " = ?", whereArray, null, null, null);
        while (paymentCR.moveToNext()) {
            String paymentMethod = paymentCR.getString(0);
            if (!paymentMethods.contains(paymentMethod))
                paymentMethods.add(paymentMethod);
        }
        database.close();
        return paymentMethods;
    }

    public void clearData() {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(CUSTOMER_TABLE, null, null);
        database.delete(INVOICE_TABLE, null, null);
        database.delete(PACKAGE_TABLE, null, null);
        database.delete(PRESCRIPTION_TABLE, null, null);
        database.delete(PAYMENT_TABLE, null, null);
        database.close();
    }

    public void saveData(JSONArray array) {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(CUSTOMER_TABLE, null, null);
        database.delete(INVOICE_TABLE, null, null);
        database.delete(PACKAGE_TABLE, null, null);
        database.delete(PRESCRIPTION_TABLE, null, null);
        database.delete(PAYMENT_TABLE, null, null);
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject customerObject = array.getJSONObject(i);
                ContentValues customerCV = new ContentValues();
                String id = "", name = "", address = "", supplierName = "", lat = "", lon = "", pending = "";
                if (customerObject.has(CUSTOMER_ID))
                    id = customerObject.getString(CUSTOMER_ID);
                if (customerObject.has(CUSTOMER_LAT))
                    lat = customerObject.getString(CUSTOMER_LAT);
                if (customerObject.has(CUSTOMER_LNG))
                    lon = customerObject.getString(CUSTOMER_LNG);
                if (customerObject.has(CUSTOMER_NAME))
                    name = customerObject.getString(CUSTOMER_NAME);
                if (customerObject.has(CUSTOMER_ADDRESS))
                    address = customerObject.getString(CUSTOMER_ADDRESS);
                if (customerObject.has(CUSTOMER_PENDING))
                    pending = customerObject.getString(CUSTOMER_PENDING);
                if (customerObject.has(CUSTOMER_SUPPLIER))
                    supplierName = customerObject.getString(CUSTOMER_SUPPLIER);
                customerCV.put(CUSTOMER_ID, id);
                customerCV.put(CUSTOMER_LAT, lat);
                customerCV.put(CUSTOMER_LNG, lon);
                customerCV.put(CUSTOMER_NAME, name);
                customerCV.put(CUSTOMER_ADDRESS, address);
                customerCV.put(CUSTOMER_PENDING, pending);
                customerCV.put(CUSTOMER_SUPPLIER, supplierName);
                database.insert(CUSTOMER_TABLE, null, customerCV);
                if (customerObject.has("payment_methods")) {
                    JSONArray paymentArray = customerObject.getJSONArray("payment_methods");
                    for (int k = 0; k < paymentArray.length(); k++) {
                        ContentValues paymentCV = new ContentValues();
                        paymentCV.put(CUSTOMER_ID, id);
                        paymentCV.put(PAYMENT_METHOD, paymentArray.getString(k));
                        database.insert(PAYMENT_TABLE, null, paymentCV);
                    }
                }
                if (customerObject.has("invoice")) {
                    JSONArray invoicesArray = customerObject.getJSONArray("invoice");
                    for (int j = 0; j < invoicesArray.length(); j++) {
                        JSONObject invoiceObject = invoicesArray.getJSONObject(j);
                        ContentValues invoiceCV = new ContentValues();
                        String invoiceId = "", amount = "", hasPackage = "", hasPrescription = "";
                        if (invoiceObject.has(INVOICE_ID))
                            invoiceId = invoiceObject.getString(INVOICE_ID);
                        if (invoiceObject.has(INVOICE_AMOUNT))
                            amount = invoiceObject.getString(INVOICE_AMOUNT);
                        if (invoiceObject.has(INVOICE_PACKAGE))
                            hasPackage = invoiceObject.getString(INVOICE_PACKAGE);
                        if (invoiceObject.has(INVOICE_PRESCRIPTION))
                            hasPrescription = invoiceObject.getString(INVOICE_PRESCRIPTION);
                        invoiceCV.put(INVOICE_ID, invoiceId);
                        invoiceCV.put(INVOICE_AMOUNT, amount);
                        invoiceCV.put(INVOICE_PACKAGE, hasPackage);
                        invoiceCV.put(INVOICE_PRESCRIPTION, hasPrescription);
                        invoiceCV.put(CUSTOMER_ID, id);
                        database.insert(INVOICE_TABLE, null, invoiceCV);
                        if (invoiceObject.has("packages")) {
                            JSONArray packagesArray = invoiceObject.getJSONArray("packages");
                            for (int k = 0; k < packagesArray.length(); k++) {
                                JSONObject packageObject = packagesArray.getJSONObject(k);
                                ContentValues packageCV = new ContentValues();
                                String item = "", quantity = "";
                                if (packageObject.has(PACKAGE_ITEM))
                                    item = packageObject.getString(PACKAGE_ITEM);
                                if (packageObject.has(PACKAGE_QUANTITY))
                                    quantity = String.valueOf(packageObject.getDouble(PACKAGE_QUANTITY));
                                packageCV.put(INVOICE_ID, invoiceId);
                                packageCV.put(PACKAGE_ITEM, item);
                                packageCV.put(PACKAGE_QUANTITY, quantity);
                                database.insert(PACKAGE_TABLE, null, packageCV);
                            }
                        }
                        if (invoiceObject.has("prescription_images")) {
                            JSONArray prescriptionArray = invoiceObject.getJSONArray("prescription_images");
                            for (int k = 0; k < prescriptionArray.length(); k++) {
                                ContentValues prescriptionCV = new ContentValues();
                                prescriptionCV.put(INVOICE_ID, invoiceId);
                                prescriptionCV.put(PRESCRIPTION_URL, prescriptionArray.getString(k));
                                database.insert(PRESCRIPTION_TABLE, null, prescriptionCV);
                            }
                        }
                        if (invoiceObject.has("payment_methods")) {
                            JSONArray paymentArray = invoiceObject.getJSONArray("payment_methods");
                            for (int k = 0; k < paymentArray.length(); k++) {
                                ContentValues paymentCV = new ContentValues();
                                paymentCV.put(CUSTOMER_ID, id);
                                paymentCV.put(PAYMENT_METHOD, paymentArray.getString(k));
                                database.insert(PAYMENT_TABLE, null, paymentCV);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }
}
