package com.c2info.deliverytrack.callbacks;

import com.c2info.deliverytrack.models.Invoice;

/**
 * Created by sai on 2/15/2017.
 */

public interface RejectedInvoiceCheckCallBack {
    public abstract void onCheckChanged(Invoice invoice, boolean isChecked);
}
