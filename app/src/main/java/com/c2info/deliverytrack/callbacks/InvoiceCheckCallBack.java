package com.c2info.deliverytrack.callbacks;

import com.c2info.deliverytrack.models.Invoice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 2/14/2017.
 */

public interface InvoiceCheckCallBack {
    void onCheckChanged(ArrayList<Invoice> invoices);
    void onClick(Invoice invoice);
    void onReject(Invoice invoice);
}
