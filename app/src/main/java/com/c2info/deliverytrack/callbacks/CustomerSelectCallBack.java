package com.c2info.deliverytrack.callbacks;

import com.c2info.deliverytrack.models.Customer;

/**
 * Created by sai on 2/21/2017.
 */

public interface CustomerSelectCallBack {
    void onSelect(Customer customer);
}
