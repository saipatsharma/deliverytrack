package com.c2info.deliverytrack.callbacks;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by sai on 2/16/2017.
 */

public interface LocationCallBack {
    void onLocationUpdate(Location location);
}
