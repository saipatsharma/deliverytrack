package com.c2info.deliverytrack.callbacks;

/**
 * Created by sai on 2/22/2017.
 */

public interface NavigationCallback {
    void onLeft();
    void onRight();
}
