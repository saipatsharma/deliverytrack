package com.c2info.deliverytrack.callbacks;

/**
 * Created by sai on 2/23/2017.
 */

public interface SignatureCallback {
    void onSignature();
}
