package com.c2info.deliverytrack.callbacks;

import com.c2info.deliverytrack.models.Package;

import java.util.List;

/**
 * Created by sai on 2/15/2017.
 */

public interface PackageCheckCallBack {
    public abstract void onCheckChanged(List<Package> packages);
}
