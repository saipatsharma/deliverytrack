package com.c2info.deliverytrack.callbacks;

/**
 * Created by sai on 2/16/2017.
 */

public interface LocationServicesEnabledCallBack {
    void onEnabled();
}
