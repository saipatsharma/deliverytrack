package com.c2info.deliverytrack.events;

/**
 * Created by sai on 2/16/2017.
 */

public class OtpReceivedEvent {
    private String otp;

    public OtpReceivedEvent(String otp) {
        this.otp = otp;
    }

    public String getOtp() {
        return otp;
    }
}
