package com.c2info.deliverytrack.events;

/**
 * Created by sai on 2/10/2017.
 */

public class GPSToggleEvent {
    private boolean isGpsOn;

    public GPSToggleEvent(boolean isGpsOn) {
        this.isGpsOn = isGpsOn;
    }

    public boolean isGpsOn() {
        return isGpsOn;
    }
}
