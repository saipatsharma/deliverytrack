package com.c2info.deliverytrack.events;

import android.location.Location;

/**
 * Created by sai on 2/21/2017.
 */

public class LocationEvent {
    private Location location;

    public LocationEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
