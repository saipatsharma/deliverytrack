package com.c2info.deliverytrack.events;

/**
 * Created by sai on 2/10/2017.
 */

public class InternetToggleEvent {
    private boolean isInternetOn;

    public InternetToggleEvent(boolean isInternetOn) {
        this.isInternetOn = isInternetOn;
    }

    public boolean isInternetOn() {
        return isInternetOn;
    }
}
